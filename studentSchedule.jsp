<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  
  /* current quarter and year */
  String quarter = "SPRING";
  int year = 2009;
  
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord = null;
  ResultSet rsSelectRecord = null;
  String sqlSelectRecord =null;
  String sqlInsertRecord = null;
  String pid =request.getParameter("Student_ID");
 
   
  // Set any empty strings to null so that sql doesn't accept bad requests
  if (pid.length()!=9){ pid = null;}
		

  String buttonDec = request.getParameter("Submit"); 
       
  try{
   
   if( buttonDec.equals(null)){ 
       System.out.println("buttonDec was nullllll");
   
   }else if(buttonDec.equals("request")){	
	   sqlSelectRecord = "Select m.mID,c.Title,c.Course_offering as Course, m.cID,x.title as conTitle, x.Course_offering " +
                         "From (select distinct m.Section_ID as mID, c.Section_ID as cID " +
                                "from ClassMeetings c,enrolled e,( " +
                                   "select m.ID,e.Section_ID " +
                                   "from Meetings m, meetings n, enrolled e, ClassMeetings c, classes x " +
                                   "where e.Student_ID = ? and e.Section_ID = c.Section_ID and x.Section_ID = e.Section_ID and x.Class_Year = ? and x.quarter = ? and " +
                                   "c.Meet_ID = n.id and ((m.startTime <=n.startTime and n.startTime < m.endTime) or (n.startTime <= m.startTime and m.startTime < n.endTime)) and m.id <> n.id and " +
                                   "m.Meeting_Days = n.Meeting_Days) as m " +
                                "where m.ID = c.Meet_ID) as m, classes c, classes x " +
                         "where c.Section_ID = m.mID and x.Section_ID = m.cID";
						
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,pid);	
	   psInsertRecord.setInt(2,year);
	   psInsertRecord.setString(3,quarter);
	   rsSelectRecord=psInsertRecord.executeQuery();

	   

	
	   
   } 
  }catch(Exception e){
	  if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}
      if(conn!=null){ conn.close();}
	  e.printStackTrace();
      response.sendRedirect("report.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="report.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br></br> 
   <br></br>
   <br></br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Concentrations completed that satisfies gpa and units requirements  </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Class ID</th>
				<th>Class Title</th>
				<th>Class Course num</th>
				<th>Conflicting ID</th>
				<th>Conflicting Title</th>
				<th>Conflicting Course num</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("mID")%> </td>
              <td><%=rsSelectRecord.getString("Title")%> </td>
              <td><%=rsSelectRecord.getString("Course")%> </td>
			  <td><%=rsSelectRecord.getString("cID")%> </td>
			  <td><%=rsSelectRecord.getString("conTitle")%> </td>	
              <td><%=rsSelectRecord.getString("Course_offering")%> </td>			  
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
	   </tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
				
	</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
  </body>
</html>
<%
    try{
      if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}
      if(conn!=null){ conn.close();}
    }catch(Exception e){
      e.printStackTrace(); 
    }
%>
<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
        
  String StudentID =request.getParameter("Student_ID");
  String SectionAsString = request.getParameter("Section");
  String GradeO = request.getParameter("GradeO");
  int Section = 0;
  try {
	// Set any empty strings to null so that sql doesn't accept bad requests
	if (StudentID.length()!=9)
	    StudentID = null;
	if (GradeO.length() == 0) 
		GradeO = null;		
	Section = Integer.parseInt(SectionAsString); 
  } catch(NumberFormatException badSSN){
	  System.out.println("Do something with this!!!");
  }

  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	   sqlInsertRecord="Select count(*) from Enrolled where Section_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,Section);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   int count = 0;
	   while (rsSelectRecord.next()){
         count = rsSelectRecord.getInt(1);
       }
	   sqlInsertRecord="Select Enrollment_Limit from Classes where Section_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,Section);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   int enrollLim = 0;
	   while (rsSelectRecord.next()){
	      enrollLim = rsSelectRecord.getInt("Enrollment_Limit");
	   }
	   if (count < enrollLim){
	      sqlInsertRecord="insert into Enrolled (Student_ID, Section_ID, Grade_Option, Grade_Reveived) values(?,?,?,NULL)";
	      psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	      psInsertRecord.setString(1,StudentID);
	      psInsertRecord.setInt(2,Section);
	      psInsertRecord.setString(3,GradeO);
				
	      psInsertRecord.executeUpdate();
	   
	      sqlSelectRecord ="SELECT * FROM Enrolled where Student_ID = ? ";
	      psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	      psInsertRecord.setString(1,StudentID);
		  rsSelectRecord=psInsertRecord.executeQuery();
	   } else {
		  sqlInsertRecord="insert into Waitlist (Student_ID, Section_ID, Grade_Option) values(?,?,?)";
	      psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	      psInsertRecord.setString(1,StudentID);
	      psInsertRecord.setInt(2,Section);
	      psInsertRecord.setString(3,GradeO);
				
	      psInsertRecord.executeUpdate();
	   }
	   sqlSelectRecord ="SELECT Student_ID, Section_ID, Grade_Option,? as status FROM Enrolled where Student_ID = ? union SELECT Student_ID, Section_ID, Grade_Option,? as status FROM Waitlist where Student_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,"Enrolled");
	   psInsertRecord.setString(2,StudentID);
	   psInsertRecord.setString(3,"Waitlist");
	   psInsertRecord.setString(4,StudentID);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   else if(buttonDec.equals("update")){
	   sqlSelectRecord ="SELECT count(*) FROM Enrolled where Student_ID = ? and Section_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,StudentID);
	   psInsertRecord.setInt(2,Section);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   int count = 0;
	   while (rsSelectRecord.next()){
	      count = rsSelectRecord.getInt(1);
	   }
	   if (count == 1){
	     sqlInsertRecord="update Enrolled set Grade_Option = ? where Student_ID = ? and Section_ID = ?";
	     psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	     psInsertRecord.setString(1,GradeO);
	     psInsertRecord.setString(2,StudentID);
	     psInsertRecord.setInt(3,Section);
				
	     psInsertRecord.executeUpdate();
       } else {
	     sqlInsertRecord="update Waitlist set Grade_Option = ? where Student_ID = ? and Section_ID = ?";
	     psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	     psInsertRecord.setString(1,GradeO);
	     psInsertRecord.setString(2,StudentID);
	     psInsertRecord.setInt(3,Section);		   
	   }
	   sqlSelectRecord ="SELECT Student_ID, Section_ID, Grade_Option,? as status FROM Enrolled where Student_ID = ? union SELECT Student_ID, Section_ID, Grade_Option,? as status FROM Waitlist where Student_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,"Enrolled");
	   psInsertRecord.setString(2,StudentID);
	   psInsertRecord.setString(3,"Waitlist");
	   psInsertRecord.setString(4,StudentID);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   else if(buttonDec.equals("delete")){
	   try {
	     sqlInsertRecord="Delete from Enrolled where Student_ID = ? and Section_ID = ?";
	     psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	     psInsertRecord.setString(1,StudentID);
	     psInsertRecord.setInt(2,Section);
	     psInsertRecord.executeUpdate();
	   }
	   catch (SQLException e){
		   try{
			   sqlInsertRecord="Delete from Waitlist where Student_ID = ? and Section_ID = ?";
	           psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	           psInsertRecord.setString(1,StudentID);
	           psInsertRecord.setInt(2,Section);
	           psInsertRecord.executeUpdate(); 
		   } 
		   catch (SQLException f){
			   throw new SQLException();
		   }
	   }
	   sqlSelectRecord ="SELECT Student_ID, Section_ID, Grade_Option,? as status FROM Enrolled where Student_ID = ? union SELECT Student_ID, Section_ID, Grade_Option,? as status FROM Waitlist where Student_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,"Enrolled");
	   psInsertRecord.setString(2,StudentID);
	   psInsertRecord.setString(3,"Waitlist");
	   psInsertRecord.setString(4,StudentID);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   
  }
  catch(Exception e)
  {
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>Section ID</th>
				<th>Grade Option</th>
				<th>Status</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Student_ID")%> </td>
			  <td><%=rsSelectRecord.getString("Section_ID")%> </td>
			  <td><%=rsSelectRecord.getString("Grade_Option")%> </td>
			  <td><%=rsSelectRecord.getString("status")%> </td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
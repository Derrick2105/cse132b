<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  //1st if statement
  PreparedStatement psInsert1Record = null; 
  ResultSet rsSelect1Record = null; 
  String sqlSelect1Record = null; 
  String sqlInsert1Record = null; 
  
  //2nd if statement 
  PreparedStatement psInsert2Record = null; 
  ResultSet rsSelect2Record = null; 
  String sqlSelect2Record = null; 
  String sqlInsert2Record = null;
  
  PreparedStatement psInsert22Record = null; 
  ResultSet rsSelect22Record = null; 
  String sqlSelect22Record = null; 
  String sqlInsert22Record = null;
  
  //3rd if statement
  PreparedStatement psInsert3Record = null; 
  ResultSet rsSelect3Record = null; 
  String sqlSelect3Record = null; 
  String sqlInsert3Record = null;
  
  String courseID =request.getParameter("CourseID");
  String professor = request.getParameter("professor");
  String quarter = request.getParameter("quarter");
  String year = request.getParameter("year");  
  
  

  

  // Set any empty strings to null so that sql doesn't accept bad requests
  if (courseID.length() ==0 ){ courseID = null;}
  if(professor.length() ==0){ professor =null; }
  if(quarter.length() == 0){quarter = null; }
  if(year.length() == 0){year = null; }
		

  String buttonDec = request.getParameter("Submit"); 
       
  try{
   
   if( buttonDec.equals(null)){ 
       System.out.println("buttonDec was nullllll");
   
   }else if(buttonDec.equals("request")){	
		//3 ii
		if(courseID != null && professor != null && quarter != null){ 
		   sqlSelect1Record =
				"Select c.grade, c.gradeCount"+
				" From CPQG c "+
				" Where c.Course_ID = ? and c.Instructor = ? and c.quarter = ? " +
					"	and c.year = ? "+
				" Group by c.grade, c.gradeCount";
		   
		   psInsert1Record = conn.prepareStatement(sqlSelect1Record); 
		   psInsert1Record.setString(2,professor);
		   psInsert1Record.setString(1,courseID); 
		   psInsert1Record.setString(3,quarter); 
		   psInsert1Record.setInt(4,Integer.parseInt(year)); 
		   rsSelect1Record=psInsert1Record.executeQuery(); 
		}    
		//3 iii and v
		else if (courseID != null && professor != null){
			//gradeCount
			sqlSelect2Record = 
				"Select c.grade,SUM(c.gradeCount) as gradeCount "+
				  "From CPG c "+
				  "Where c.Course_ID = ? and c.Instructor = ? "+ 
				  "Group by c.grade ";
		   psInsert2Record = conn.prepareStatement(sqlSelect2Record); 
		   psInsert2Record.setString(2,professor);
		   psInsert2Record.setString(1,courseID);  
		   rsSelect2Record=psInsert2Record.executeQuery();  
		   
		   
		   sqlSelect22Record = "Select f.Name, sum(e.units*g.NUMBER_GRADE)/sum(e.units) as total "+
				"from faculty f, enrolled e, classes c, grade_conversion g "+ 
				"where f.Title = 'professor' and f.Name = ? and c.Class_Instructor = f.Name "+
				"and c.Section_ID = e.Section_ID and c.Course_offering= ? and g.LETTER_GRADE = e.Grade_Reveived "+
				"group by f.Name";
           psInsert22Record = conn.prepareStatement(sqlSelect22Record); 
		   psInsert22Record.setString(1,professor);
		   psInsert22Record.setString(2,courseID);  
		   rsSelect22Record=psInsert22Record.executeQuery(); 
		
		}
		//3 iv
		else if(courseID != null){
		   sqlSelect3Record = 
				"Select m.Grade , count(m.count) as count "+
				"From (Select case"+
				" When m.Grade_Reveived LIKE 'A%' then 'A' "+
				" When m.Grade_Reveived LIKE 'B%' then 'B' "+
				" When m.Grade_Reveived LIKE 'C%' then 'C' "+
				" When m.Grade_Reveived LIKE 'D%' then 'D' "+
				" else 'Other' "+
				" end as Grade, 1 as count"+
				" from (select distinct c.Class_Instructor, e.Grade_Reveived, e.Units"+
				" From enrolled e, classes c, Courses x"+
				" where c.Section_ID = e.Section_ID  and"+ 
				" c.Course_offering = ? and e.Grade_Reveived is not null) m) as m "+
				"group by m.Grade"; 
           psInsert3Record = conn.prepareStatement(sqlSelect3Record); 
		   psInsert3Record.setString(1,courseID);  
		   rsSelect3Record=psInsert3Record.executeQuery(); 
		
		}
	
	
	   
   } 
  }catch(Exception e){
	  if(psInsert1Record!=null){psInsert1Record.close();}
	  if(rsSelect1Record!=null){rsSelect1Record.close();}
      if(conn!=null){ conn.close();}
	  e.printStackTrace();
      response.sendRedirect("report.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="report.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br></br> 
   <br></br>
   <br></br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Grade Count given Instuctor, CourseID, Quarter</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Grade</th>
				<th>Count</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelect1Record !=null && rsSelect1Record.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelect1Record.getString("Grade")%> </td>
			  <td><%=rsSelect1Record.getString("gradeCount")%> </td>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
	   </tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
	
	<div class="panel panel-danger">
	 <div class="panel-heading">
		<h3 class="panel-title">Grade Count given CourseID and Instuctor</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Grade</th>
				<th>Count</th>
			  </tr>
			</thead>
			<tbody>
	<%
	   cnt=1;
	  while(rsSelect2Record != null && rsSelect2Record.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelect2Record.getString("Grade")%> </td>
			  <td><%=rsSelect2Record.getString("gradeCount")%> </td>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
	   </tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
		
		<div class="panel panel-warning">
	 <div class="panel-heading">
		<h3 class="panel-title">GPA given CourseID and Instuctor of a certain course taught</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Professor</th>
				<th>GPA</th>
			  </tr>
			</thead>
			<tbody>
	<%
	   cnt=1;
	  while( rsSelect2Record != null && rsSelect22Record.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelect22Record.getString("Name")%> </td>
			  <td><%=rsSelect22Record.getString("total")%> </td>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
	   </tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
		
		<div class="panel panel-primary">
	 <div class="panel-heading">
		<h3 class="panel-title">Grade given CourseID</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Grade</th>
				<th>Count</th>
			  </tr>
			</thead>
			<tbody>
	<%
	   cnt=1;
	  while( rsSelect3Record != null && rsSelect3Record.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelect3Record.getString("Grade")%> </td>
			  <td><%=rsSelect3Record.getString("count")%> </td>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
	   </tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
	
	</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
  </body>
</html>
<%
    try{
      if(psInsert1Record!=null){ psInsert1Record.close();}
	  if(rsSelect1Record!=null){ rsSelect1Record.close();}
      if(conn!=null){ conn.close();}
    }catch(Exception e){
      e.printStackTrace(); 
    }
%>
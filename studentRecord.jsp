<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord1=null;
  PreparedStatement psInsertRecord2=null;
  ResultSet rsSelectRecord1 =null;
  ResultSet rsSelectRecord2 =null;
  String sqlSelectRecord =null;

  
  String sqlInsertRecord=null;
        
  String StudentID =request.getParameter("Student_ID");
  String FirstName = request.getParameter("First_Name");
  String Middle = request.getParameter("Middle_Initial");
  String LastName = request.getParameter("Last_Name");
  String SSN = request.getParameter("SSN");
  String Enrolled = request.getParameter("Enrolled");
  String Period = request.getParameter("Period_Of_Attendance");
  String Residency = request.getParameter("Residency");
  String College = null;
  String MSBS = null;
  String Gtype = null;
  String D_ID= null;
  Boolean UnderGrad = false;
  Boolean Grad = false;
  int GradDept = 0;
  if (request.getParameter("undergrad") != null){
	  College = request.getParameter("college");
	  if (College.length() == 0)
		  College = null;
	  UnderGrad = true;
	  Grad = false;
	  if (request.getParameter("ms/bs") != null){
		  MSBS = "y";
	  }
  } else if(request.getParameter("grad") != null){
	  Grad = true;
	  UnderGrad = false;
	  Gtype = request.getParameter("type_of_graduate");
	  if (Gtype.length()== 0)
		 Gtype = null;
	  D_ID= request.getParameter("department_id");
}
  try {
	// Set any empty strings to null so that sql doesn't accept bad requests
	if (StudentID.length()!=9)
	    StudentID = null;
	if (FirstName.length()==0)
		FirstName = null;
	if (Middle.length() == 0) 
		Middle = null;
	if (LastName.length() == 0)
        LastName = null;		
	if (Enrolled.length() == 0)
        Enrolled = null;		
	if (Residency.length() == 0)
        Residency = null;		
	GradDept = Integer.parseInt(D_ID);
	Integer.parseInt(SSN); 
  } catch(NumberFormatException badSSN){
	  System.out.println("Do something with this!!!");
  }

  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
   
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	   sqlInsertRecord="insert into student (Student_ID, First_Name, Middle_Initial, Last_Name, SSN,Enrolled,Per_Of_Attendance,Residency ) values(?,?,?,?,?,?,?,?)";
	   psInsertRecord1=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord1.setString(1,StudentID);
	   psInsertRecord1.setString(2,FirstName);
	   psInsertRecord1.setString(3,Middle);
	   psInsertRecord1.setString(4,LastName);
	   psInsertRecord1.setString(5,SSN);
	   psInsertRecord1.setString(6,Enrolled);
	   psInsertRecord1.setString(7,Period);
	   psInsertRecord1.setString(8,Residency);
	   System.out.println(StudentID);		
	   if (UnderGrad){
		   sqlInsertRecord="insert into Undergraduate (Undergraduate_ID, MS_BS, College) values(?,?,?)";
	       psInsertRecord2=conn.prepareStatement(sqlInsertRecord);
	       psInsertRecord2.setString(1,StudentID);
	       psInsertRecord2.setString(2,MSBS);
		   psInsertRecord2.setString(3,College);
		   System.out.println("StudentID: " + StudentID + " MSBS: " + MSBS + " College " + College);
	   } else if (Grad){
		   sqlInsertRecord="insert into Graduate (Graduate_ID, Graduate_Type, Department_ID, Grad_Thesis_Committee) values(?,?,?,null)";
	       psInsertRecord2=conn.prepareStatement(sqlInsertRecord);
	       psInsertRecord2.setString(1,StudentID);
	       psInsertRecord2.setString(2,Gtype);  
		   psInsertRecord2.setInt(3,GradDept);  
	   } else 
		   throw new SQLException();
	   conn.setAutoCommit(false);
	   psInsertRecord1.executeUpdate();
	   psInsertRecord2.executeUpdate();
	   conn.commit();
	   conn.setAutoCommit(true);
	   sqlSelectRecord ="SELECT * FROM student";
	   psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord1=psInsertRecord1.executeQuery();
	   
   }
   else if(buttonDec.equals("update")){
	   sqlInsertRecord="update student set First_Name = ?, Middle_Initial =? , Last_Name = ? , SSN = ?,Enrolled =?,Per_Of_Attendance=?,Residency=? where Student_ID = ?";
	   psInsertRecord1=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord1.setString(1,FirstName);
	   psInsertRecord1.setString(2,Middle);
	   psInsertRecord1.setString(3,LastName);
	   psInsertRecord1.setString(4,SSN);
	   psInsertRecord1.setString(5,Enrolled);
	   psInsertRecord1.setString(6,Period);
	   psInsertRecord1.setString(7,Residency);
	   psInsertRecord1.setString(8,StudentID);
       conn.setAutoCommit(false);
	   if (UnderGrad){
		   sqlInsertRecord="update Undergraduate set MS_BS = ?, College = ? where Undergraduate_ID = ?";
	       psInsertRecord2=conn.prepareStatement(sqlInsertRecord);
	       psInsertRecord2.setString(1,MSBS);
		   psInsertRecord2.setString(2,College);
		   psInsertRecord2.setString(3,StudentID);
	   } else if (Grad){
		   sqlInsertRecord="update Graduate set Graduate_Type = ?, Department_ID = ? where Graduate_ID = ?";
	       psInsertRecord2=conn.prepareStatement(sqlInsertRecord);
	       psInsertRecord2.setString(3,StudentID);
	       psInsertRecord2.setString(1,Gtype);  
		   psInsertRecord2.setInt(2,GradDept);  
	   } else 
		   throw new SQLException();
	   psInsertRecord1.executeUpdate();
	   psInsertRecord2.executeUpdate();
	   conn.commit();
	   conn.setAutoCommit(true);
	   
	   sqlSelectRecord ="SELECT * FROM student";
	   psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord1=psInsertRecord1.executeQuery();
	   System.out.println("I entered update"); 
   }
   else if(buttonDec.equals("delete")){
	   sqlInsertRecord="Delete from student where Student_ID = ?";
	   psInsertRecord1=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord1.setString(1,StudentID);
	   psInsertRecord1.executeUpdate();
	   
	   sqlSelectRecord ="SELECT * FROM student";
	   psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord1=psInsertRecord1.executeQuery();
   }
   
  }
  catch(Exception e)
  {
	  if (conn != null) {
         System.err.print("Transaction is being rolled back");
         conn.rollback();
      }
	  conn.setAutoCommit(true);
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>First Name</th>
				<th>Middle initial</th>
				<th>Last Name</th>
				<th>SSN</th>
				<th>Enrolled</th>
				<th>Period of Attendance</th>
				<th>Residency</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord1.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord1.getString("Student_ID")%> </td>
			  <td><%=rsSelectRecord1.getString("First_Name")%> </td>
			  <td><%=rsSelectRecord1.getString("Middle_Initial")%> </td>
			  <td><%=rsSelectRecord1.getString("Last_Name")%></td>
			  <td><%=rsSelectRecord1.getString("SSN")%></td>
			  <td><%=rsSelectRecord1.getString("Enrolled")%></td>
			  <td><%=rsSelectRecord1.getString("Per_Of_Attendance")%></td>
			  <td><%=rsSelectRecord1.getString("Residency")%></td>

			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord1!=null)
      {
       psInsertRecord1.close();
      }
      if(psInsertRecord2!=null)
      {
       psInsertRecord2.close();
      }
	  if(rsSelectRecord1!=null)
      {
        rsSelectRecord1.close();
      }
	  if(rsSelectRecord2!=null)
      {
        rsSelectRecord2.close();
      }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
<%@ page language="java" import="java.sql.*" errorPage="" %>
<%
  Connection conn = null;
  try{
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      conn = DriverManager.getConnection("jdbc:sqlserver://DERRICK-PC;instanceName=CSE132B;databaseName=csee132b","cse132bProject", "cse132b");
  } catch (SQLException sqle) {
	  Class.forName("com.mysql.jdbc.Driver").newInstance();
      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/csee132b","armando", "password");
  }
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
        
  String StudentID =request.getParameter("Student_ID");
  String Name1 = request.getParameter("Faculty1");
  String Name2 = request.getParameter("Faculty2");
  String Name3 = request.getParameter("Faculty3");
  String Name4 = null;
  int ID = 0;
  boolean phd = false;
  if (request.getParameter("PHDCand") != null){
	  phd = true;
	Name4 = request.getParameter("Faculty4");
		if (Name4.length() == 0)
        Name4 = null;	
  }
  String IDAsString = request.getParameter("ID");
  try {
	// Set any empty strings to null so that sql doesn't accept bad requests
	if (StudentID.length()!=9)
	    StudentID = null;
	if (Name2.length()==0)
		Name2 = null;
	if (Name3.length() == 0) 
		Name3 = null;	
	if (Name1.length() == 0)
        Name1 = null;				
	
	 ID = Integer.parseInt(IDAsString); 
  } catch(NumberFormatException e){
	  System.out.println("Do something with this!!!");
  }

  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	conn.setAutoCommit(false);
	
	   sqlInsertRecord="Insert into ThesisCommittee (Com_ID) VALUES (?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,ID);		
	   psInsertRecord.executeUpdate();
	   
	   
	   sqlInsertRecord="update Graduate set Grad_Thesis_Committee = ? where Graduate_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,ID);	
	   psInsertRecord.setString(2,StudentID);
	   
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="SELECT Department_ID FROM Graduate where Graduate_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,StudentID);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   int dept = 0;
	   while(rsSelectRecord.next()){
	      dept = rsSelectRecord.getInt("Department_ID");
	   }
	   int dep = 0;
	   sqlSelectRecord ="SELECT Department_Num FROM Faculty_Dept where Faculty_Name = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,Name1);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   while(rsSelectRecord.next()){
	     dep = rsSelectRecord.getInt("Department_Num");
	   }
	   if (dep != dept){
		  throw new SQLException(); 
	   } else {
	     //Faculty 1
	     sqlInsertRecord="Insert into ThesisCommitteeFaculty (Com_ID,Name) VALUES (?,?)";
	     psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	     psInsertRecord.setString(2,Name1);
	     psInsertRecord.setInt(1,ID);		
	     psInsertRecord.executeUpdate();
	   }
	   sqlSelectRecord ="SELECT Department_Num FROM Faculty_Dept where Faculty_Name = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,Name2);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   while(rsSelectRecord.next()){
	     dep = rsSelectRecord.getInt("Department_Num");
	   }
	   if (dep != dept){
		 throw new SQLException();  
	   } else {
	     //Faculty 2
	     sqlInsertRecord="Insert into ThesisCommitteeFaculty (Com_ID,Name) VALUES (?,?)";
	     psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	     psInsertRecord.setString(2,Name2);
	     psInsertRecord.setInt(1,ID);		
	     psInsertRecord.executeUpdate();
	   }
	   sqlSelectRecord ="SELECT Department_Num FROM Faculty_Dept where Faculty_Name = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,Name3);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   while(rsSelectRecord.next()){
	     dep = rsSelectRecord.getInt("Department_Num");
	   }
	   if (dep != dept){
		 throw new SQLException();    
	   } else {
	     //Faculty 3
	     sqlInsertRecord="Insert into ThesisCommitteeFaculty (Com_ID,Name) VALUES (?,?)";
	     psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	     psInsertRecord.setString(2,Name3);
	     psInsertRecord.setInt(1,ID);		
	     psInsertRecord.executeUpdate();
	   }
	   if (phd){
		 sqlSelectRecord ="SELECT Department_Num FROM Faculty_Dept where Faculty_Name = ?";
	     psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	     psInsertRecord.setString(1,Name4);
	     rsSelectRecord=psInsertRecord.executeQuery();
	     while(rsSelectRecord.next()){
	       dep = rsSelectRecord.getInt("Department_Num");
	     }
	   	 if (dep == dept){	 
		   throw new SQLException();
	     } else {
	       //Faculty 4
	       sqlInsertRecord="Insert into ThesisCommitteeFaculty (Com_ID,Name) VALUES (?,?)";
	       psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	       psInsertRecord.setString(2,Name4);
	       psInsertRecord.setInt(1,ID);		
	       psInsertRecord.executeUpdate();
		 }
	   }
	   conn.commit();
	   conn.setAutoCommit(true);
	   
	   sqlSelectRecord ="SELECT g.Graduate_ID,g.Grad_Thesis_Committee as ID, f.Name as Name FROM Graduate g, ThesisCommitteeFaculty f where f.Com_ID = g.Grad_Thesis_Committee and g.Grad_Thesis_Committee = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,ID);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   
   }
   else if(buttonDec.equals("update")){  
       conn.setAutoCommit(false);
	   System.out.println("Here1");
	   //Faculty 1
	   sqlInsertRecord="update ThesisCommitteeFaculty set Name = ? where Com_ID = ? AND NOT EXists (select * from ThesisCommitteeFaculty where Name = ? and Com_ID = ?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,Name1);
	   psInsertRecord.setInt(2,ID);	
       psInsertRecord.setString(3,Name1);
	   psInsertRecord.setInt(4,ID);			   
	   psInsertRecord.executeUpdate();
	   System.out.println("Here2");
	   //Faculty 2
	   sqlInsertRecord="update ThesisCommitteeFaculty set Name = ? where Com_ID = ? AND NOT EXists (select * from ThesisCommitteeFaculty where Name = ? and Com_ID = ?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,Name1);
	   psInsertRecord.setInt(2,ID);	
       psInsertRecord.setString(3,Name1);
	   psInsertRecord.setInt(4,ID);		
	   psInsertRecord.executeUpdate();
	   System.out.println("Here3");
	   //Faculty 3
	   sqlInsertRecord="update ThesisCommitteeFaculty set Name = ? where Com_ID = ? AND NOT EXists (select * from ThesisCommitteeFaculty where Name = ? and Com_ID = ?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,Name1);
	   psInsertRecord.setInt(2,ID);	
       psInsertRecord.setString(3,Name1);
	   psInsertRecord.setInt(4,ID);		
	   psInsertRecord.executeUpdate();
	   System.out.println(phd);
	   if (phd){
	     //Faculty 4
		 System.out.println("Here4");
	     sqlInsertRecord="update ThesisCommitteeFaculty set Name = ? where Com_ID = ? AND NOT EXists (select * from ThesisCommitteeFaculty where Name = ? and Com_ID = ?)";
	     psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	     psInsertRecord.setString(1,Name1);
	     psInsertRecord.setInt(2,ID);	
         psInsertRecord.setString(3,Name1);
	     psInsertRecord.setInt(4,ID);		
	     psInsertRecord.executeUpdate();
	   }
	   System.out.println("Here5");
	   System.out.println(ID);
	   sqlSelectRecord ="SELECT g.Graduate_ID,g.Grad_Thesis_Committee as ID, f.Name as Name FROM Graduate g, ThesisCommitteeFaculty f where f.Com_ID = g.Grad_Thesis_Committee and g.Grad_Thesis_Committee = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,ID);	
	   rsSelectRecord=psInsertRecord.executeQuery();
	   conn.commit();
	   conn.setAutoCommit(true);
   }
   else if(buttonDec.equals("delete")){
	   sqlInsertRecord="Delete from ThesisCommittee where Com_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,ID);
	   psInsertRecord.executeUpdate();
	   
	   	   sqlSelectRecord ="SELECT g.Graduate_ID,g.Grad_Thesis_Committee as ID, f.Name as Name FROM Graduate g, ThesisCommitteeFaculty f where f.Com_ID = g.Grad_Thesis_Committee and g.Grad_Thesis_Committee = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,ID);
	   rsSelectRecord=psInsertRecord.executeQuery();

   }
   
  }
  catch(Exception e)
  {
      if (conn != null) {
         System.err.print("Transaction is being rolled back");
         conn.rollback();
      }
	  conn.setAutoCommit(true);
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return;  
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>Committee ID </th>
				<th>Faculty Name</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Graduate_ID")%> </td>
			  <td><%=rsSelectRecord.getString("ID")%> </td>
			  <td><%=rsSelectRecord.getString("Name")%> </td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
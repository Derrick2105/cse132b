<%@ page language="java" import="java.sql.*" errorPage="" %>
<%
  Connection conn = null;
  try{
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      conn = DriverManager.getConnection("jdbc:sqlserver://DERRICK-PC;instanceName=CSE132B;databaseName=csee132b","cse132bProject", "cse132b");
  } catch (SQLException sqle) {
	  Class.forName("com.mysql.jdbc.Driver").newInstance();
      conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/csee132b","armando", "password");
  }
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
        
  String StudentID =request.getParameter("Student_ID");
  String Reason = request.getParameter("Reason");
  String StartQuarter = request.getParameter("Start_Quarter");
  String StartYearAsString = request.getParameter("Start_Year");
  String EndQuarter = request.getParameter("End_Quarter");
  String EndYearAsString = request.getParameter("End_Year");
  int StartYear = 0;
  int EndYear = 0;
  try {
	// Set any empty strings to null so that sql doesn't accept bad requests
	if (StudentID.length()!=9)
	    StudentID = null;
	if (Reason.length()==0)
		Reason = null;
	if (StartQuarter.length() == 0) 
		StartQuarter = null;
	if (EndQuarter.length() == 0)
        EndQuarter = null;			
	
	StartYear = Integer.parseInt(StartYearAsString); 
	EndYear = Integer.parseInt(EndYearAsString); 
  } catch(NumberFormatException badSSN){
	  System.out.println("Do something with this!!!");
  }

  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	System.out.println("buttonDec was addddddd");
	   sqlInsertRecord="insert into Probation (Student_ID, Reason, period_start_quarter, period_start_year, period_end_quarter,period_end_year) values(?,?,?,?,?,?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,StudentID);
	   psInsertRecord.setString(2,Reason);
	   psInsertRecord.setString(3,StartQuarter);
	   psInsertRecord.setInt(4,StartYear);
	   psInsertRecord.setString(5,EndQuarter);
	   psInsertRecord.setInt(6,EndYear);
				
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="SELECT * FROM Probation";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   else if(buttonDec.equals("update")){
	   sqlInsertRecord="update Probation set Reason = ?, period_start_quarter = ?, period_start_year = ?, period_end_quarter = ?,period_end_year = ? where Student_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,Reason);
	   psInsertRecord.setString(2,StartQuarter);
	   psInsertRecord.setInt(3,StartYear);
	   psInsertRecord.setString(4,EndQuarter);
	   psInsertRecord.setInt(5,EndYear);
	   psInsertRecord.setString(6,StudentID);
				
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="SELECT * FROM Probation";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   System.out.println("I entered update"); 
   }
   else if(buttonDec.equals("delete")){
	   sqlInsertRecord="Delete from Probation where Student_ID = ? and period_start_quarter = ? and period_start_year = ? and period_end_quarter = ? and period_end_year = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,StudentID);
	   psInsertRecord.setString(2,StartQuarter);
	   psInsertRecord.setInt(3,StartYear);
	   psInsertRecord.setString(4,EndQuarter);
	   psInsertRecord.setInt(5,EndYear);
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="SELECT * FROM Probation";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   
  }
  catch(Exception e)
  {
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>Reason</th>
				<th>Start Quarter</th>
				<th>Start Year</th>
				<th>End Quarter</th>
				<th>End Year</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Student_ID")%> </td>
			  <td><%=rsSelectRecord.getString("Reason")%> </td>
			  <td><%=rsSelectRecord.getString("period_start_quarter")%> </td>
			  <td><%=rsSelectRecord.getInt("period_start_year")%></td>
			  <td><%=rsSelectRecord.getString("period_end_quarter")%></td>
			  <td><%=rsSelectRecord.getInt("period_end_year")%></td>


			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
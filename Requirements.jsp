<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
   


  String GPA =request.getParameter("Min_Cat_GPA");
  String CIDAsString = request.getParameter("C_ID");
  String DepIDAsString = request.getParameter("Department_ID");
  String DegIDAsString = request.getParameter("ID");
  String UnitsAsString = request.getParameter("Unit_Requirement");
  String CatName = request.getParameter("Category_Name");
  String MajMin = request.getParameter("Major_Minor");
  int CID = 0;
  int DegID = 0;
  int DepID = 0;
  int Units = 0;
  // Set any empty strings to null so that sql doesn't accept bad requests
  if (GPA.length()==0)
	 GPA = null;
  if (CatName.length() == 0) 
	 CatName = null;
  if (MajMin.length() == 0) 
	 MajMin = null; 

  try {
	CID = Integer.parseInt(CIDAsString);  
	DegID = Integer.parseInt(DegIDAsString);  
	DepID = Integer.parseInt(DepIDAsString); 
    Units = Integer.parseInt(UnitsAsString);  	
  } catch(NumberFormatException e){
	  System.out.println("Fix it...");
  }


  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   }
   else if(buttonDec.equals("add")){
	   sqlInsertRecord="insert into Requirements (C_ID,ID, Department_ID,Unit_Requirement ,Min_Cat_GPA,Category_Name, Major_Minor) values (?,?,?,?,?,?,?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,CID);	
       psInsertRecord.setInt(2,DegID);
       psInsertRecord.setInt(3,DepID);
       psInsertRecord.setInt(4,Units);
	   psInsertRecord.setString(5,GPA);
	   psInsertRecord.setString(6,CatName);	
       psInsertRecord.setString(7,MajMin);		   
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="Select * from Requirements where C_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,CID);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }else if(buttonDec.equals("update")){  
	   sqlSelectRecord ="Update Requirements set Unit_Requirement = ?, ID = ?, Department_ID = ?,Min_Cat_GPA = ?,Category_Name = ?,Major_Minor= ? where C_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,Units);
	   psInsertRecord.setInt(2,DegID);
       psInsertRecord.setInt(3,DepID);
	   psInsertRecord.setString(4,GPA);
	   psInsertRecord.setString(5,CatName);	
       psInsertRecord.setString(6,MajMin);	
       psInsertRecord.setInt(7,CID);		   
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="Select * from Requirements where C_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,CID);
	   rsSelectRecord=psInsertRecord.executeQuery();
   } else if(buttonDec.equals("delete")){
	   sqlSelectRecord ="Delete from Requirements where C_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,CID);
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="Select * from Requirements where C_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,CID);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   
  }
  catch(Exception e)
  {
      conn.close();
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Category Id</th>
				<th>Unit Requirement</th>
				<th>Degree ID</th>
				<th>Department Id</th>
				<th>Minimum GPA</th>
				<th>Category Name</th>
				<th>Major or Minor</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next()) 
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("C_ID")%> </td>
			  <td><%=rsSelectRecord.getString("Unit_Requirement")%> </td>
			  <td><%=rsSelectRecord.getString("ID")%> </td>
			  <td><%=rsSelectRecord.getString("Department_ID")%> </td>
			  <td><%=rsSelectRecord.getString("Min_Cat_GPA")%> </td>
			  <td><%=rsSelectRecord.getString("Category_Name")%> </td>
			  <td><%=rsSelectRecord.getString("Major_Minor")%> </td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
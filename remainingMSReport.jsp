<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord = null;
  ResultSet rsSelectRecord = null;
  String sqlSelectRecord =null;
  String sqlInsertRecord = null;
  String degreeName = request.getParameter("degreeName");
  String pid =request.getParameter("PID");
   
  PreparedStatement psInsertMSRecord = null; 
  ResultSet rsSelectMSRecord = null; 
  String sqlSelectMSRecord = null; 
  String sqlInsertMSRecord = null; 
 
  

  

  // Set any empty strings to null so that sql doesn't accept bad requests
  if (pid.length()!=9){ pid = null;}
		

  String buttonDec = request.getParameter("Submit"); 
       
  try{
   
   if( buttonDec.equals(null)){ 
       System.out.println("buttonDec was nullllll");
   
   }else if(buttonDec.equals("request")){	
	   sqlSelectRecord =	"Select c.Concentration AS C "+ 
								"From concentration c "+
								"Where c.Concentration IN "+ 
								"(Select c.Concentration"+
								" From MS m, degree d,"+
								" (Select c.Concentration,C.Course_Number, e.units"+
								" From Concentration c, Classes f, enrolled e"+
								" where e.Student_ID = ? and e.Section_ID = f.Section_ID" +
								" and c.Course_Number = f.Course_offering and e.Grade_Reveived <> 'F' and  e.Grade_Reveived <> 'NP' and e.Grade_Reveived <> 'IN'" +
								" Group by c.Concentration,C.Course_Number, e.units) as c"+
								" where c.Concentration = m.Concentration AND m.MS_ID = d.ID AND d.Name = ?" +
								" group by c.Concentration, m.Units"+
								" having m.Units - sum(c.units) <=0) AND" +
								" c.Concentration IN"+
								" (Select c.Concentration "+
								" From MS m, GRADE_CONVERSION g, degree d,"+
								" (Select c.Concentration,C.Course_Number, e.units, e.Grade_Reveived"+
								" From Concentration c, Classes f, enrolled e "+
								"where e.Student_ID = ? and e.Section_ID = f.Section_ID "+
								"and c.Course_Number = f.Course_offering and e.Grade_Reveived <> 'F' and e.Grade_Reveived <> 'NP' and e.Grade_Reveived <> 'IN' AND e.Grade_Option <> 'S/U' "+
								"Group by c.Concentration,C.Course_Number, e.units) as c "+
								"where c.Concentration = m.Concentration AND m.MS_ID = d.ID AND d.Name = ? "+
								"and g.LETTER_GRADE = c.Grade_Reveived "+
								"group by c.Concentration, m.Units "+
								"having  sum(c.units * g.NUMBER_GRADE) DIV sum(c.units) > 1.9) "+
								"Group BY c.Concentration "; 
						
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,pid);	
	   psInsertRecord.setString(2,degreeName);
	   psInsertRecord.setString(3,pid);	
	   psInsertRecord.setString(4,degreeName);
	   System.out.println("-----------------------------------------------------"); 
	   System.out.println(psInsertRecord); 
	   rsSelectRecord=psInsertRecord.executeQuery();
	   
	   sqlSelectMSRecord =	"Select Distinct (con.Course_Number), con.Concentration, cla.quarter, cla.Class_Year " +
							"from (Select d.Name ,con.Concentration, con.Course_Number " +
								" From concentration con,ms m, degree d " +
								" where  con.Course_Number Not In (Select C.Course_Number " +
										" From Concentration c, Classes f, enrolled e, ms m, degree d " +
										" where e.Student_ID = ? and e.Section_ID = f.Section_ID and e.Grade_Reveived <> 'F' " +
										" and c.Course_Number = f.Course_offering and e.Grade_Reveived <> 'NP' "+
										" and m.MS_ID = d.ID and d.Name = ? "+
										" Group by d.ID, c.Concentration,C.Course_Number) "+
									  " and m.MS_ID = d.ID and d.Name = ? "+
								" Group BY d.Name ,con.Concentration, con.Course_Number "+
								" ) as con, "+
								" (Select c.Course_offering, c.quarter, c.Class_Year "+
								" from classes c "+
								" where c.Class_Year >= 2005 AND c.quarter = 'Fall'  OR c.Class_Year > 2005 "+
								" group by c.Course_offering, c.Class_Year, c.quarter " +
								" order by c.Class_Year, case quarter "+
								 " when 'FALL' then 1 "+
								  " when 'SPRING' then 3 " +
								  " when 'WINTER' then 2 "+
								" end) as cla "+
								" Where con.Course_Number = cla.Course_offering "+
								" Group by con.Course_Number "+
								" order by cla.Class_Year, case cla.quarter "+
									"	   when 'FALL' then 1 "+
									"	   when 'SPRING' then 3 " +
									"	   when 'WINTER' then 2 "+
									"	end ";
	   
	   psInsertMSRecord = conn.prepareStatement(sqlSelectMSRecord); 
	   psInsertMSRecord.setString(1,pid);
	   psInsertMSRecord.setString(2,degreeName); 
	   psInsertMSRecord.setString(3,degreeName); 
	   rsSelectMSRecord=psInsertMSRecord.executeQuery(); 
	   

	
	   
   } 
  }catch(Exception e){
	  if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}
	  if(psInsertMSRecord!=null){psInsertMSRecord.close();}
	  if(rsSelectMSRecord!=null){rsSelectMSRecord.close();}
      if(conn!=null){ conn.close();}
	  e.printStackTrace();
      response.sendRedirect("report.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="report.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br></br> 
   <br></br>
   <br></br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Concentrations completed that satisfies gpa and units requirements  </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Concentration</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("C")%> </td>			  
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
	   </tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
	
	<div class="panel panel-primary">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Category Name with units needed for completion </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Category ID</th>
				<th>Category Name</th>
				<th>Units Left</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  cnt=1;
	  while(rsSelectMSRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectMSRecord.getString("Course_Number")%> </td>
			  <td><%=rsSelectMSRecord.getString("Concentration")%> </td>
			  <td><%=rsSelectMSRecord.getString("quarter")%> </td>
			  <td><%=rsSelectMSRecord.getString("Class_Year")%> </td>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
		</tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
	
		
		
	</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
  </body>
</html>
<%
    try{
      if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}
	  if(psInsertMSRecord!=null){ psInsertMSRecord.close();}
	  if(rsSelectMSRecord!=null){ rsSelectMSRecord.close();}
      if(conn!=null){ conn.close();}
    }catch(Exception e){
      e.printStackTrace(); 
    }
%>
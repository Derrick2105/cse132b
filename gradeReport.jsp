<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord = null;
  ResultSet rsSelectRecord = null;
  ResultSet rsSelectRecord1 = null;
  ResultSet rsSelectRecord2 = null;
  
  String sqlSelectRecord =null;
  String sqlInsertRecord = null;
        
  String PID =request.getParameter("PID");
  

  // Set any empty strings to null so that sql doesn't accept bad requests
  if (PID.length()!=9){ PID = null;}
		

  String buttonDec = request.getParameter("Submit"); 
       
  try{
   
   if( buttonDec.equals(null)){ 
       System.out.println("buttonDec was nullllll");
   
   }else if(buttonDec.equals("request")){	
	   sqlSelectRecord ="select e.Section_ID,c.Class_Instructor,c.Course_offering,c.title,c.Class_Year,c.quarter,e.Grade_Option,e.Grade_Reveived,e.Units " + 
          "From enrolled e, classes c "+
          "where e.Section_ID = c.Section_ID and e.Student_ID = ? " +
          "group by c.Class_Year, c.quarter,e.Section_ID,e.Grade_Option,e.Grade_Reveived,c.Class_Instructor,c.Course_offering,c.title, e.units " +
          "order by c.Class_Year desc, case quarter " +
             "when 'FALL' then 1 " +
             "when 'SPRING' then 3 " +
             "when 'WINTER' then 2 " +
          "end";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,PID);	
	   System.out.println(psInsertRecord); 
	   rsSelectRecord=psInsertRecord.executeQuery();
	   
	   sqlSelectRecord = "select c.Class_Year,c.quarter,sum(e.units) as units,sum(e.units*g.NUMBER_GRADE) as total " +
	                      "From enrolled e, classes c, GRADE_CONVERSION g " +
						  "Where e.Section_ID = c.Section_ID and e.Student_ID = ? and " +
						  "e.Grade_Reveived <> 'IN' and e.Grade_Reveived = g.LETTER_GRADE " +
	                      "group by c.Class_Year, c.quarter " +
                          "order by c.Class_Year desc, case quarter " +
                            "when 'FALL' then 1 " +
                            "when 'SPRING' then 3 " +
                            "when 'WINTER' then 2 " +
                          "end";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,PID);	
	   rsSelectRecord1=psInsertRecord.executeQuery(); 
	   
	   sqlSelectRecord = "select sum(e.units) as units,sum(e.units*g.NUMBER_GRADE) as total " +
	                      "From enrolled e, classes c, GRADE_CONVERSION g " +
						  "Where e.Section_ID = c.Section_ID and e.Student_ID = ? and " +
						      "e.Grade_Reveived <> 'IN' and e.Grade_Reveived = g.LETTER_GRADE ";
	                      
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,PID);	
	   rsSelectRecord2=psInsertRecord.executeQuery();
   } 
  }catch(Exception e){
	  if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}  
	  if(rsSelectRecord1!=null){ rsSelectRecord.close();}  
	  if(rsSelectRecord2!=null){ rsSelectRecord.close();}  
      if(conn!=null){ conn.close();}
	  e.printStackTrace();
      response.sendRedirect("report.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="report.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br
   <!--  This is the classes and grade received in order by quarter for student x --> 
   <div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: All Classes taken by student grouped by quarter</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th> 
				<th>Section Id</th>
				<th>Class Instructor</th>
				<th>Course Offering</th>
				<th>Class Title</th>
				<th>Year</th>
				<th>Quarter</th>
				<th>Grade Option</th>
				<th>Grade Received</th>
				<th>Units</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt =1; 
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Section_ID")%> </td>
			  <td><%=rsSelectRecord.getString("Class_Instructor")%> </td>
			  <td><%=rsSelectRecord.getString("Course_offering")%></td>
			  <td><%=rsSelectRecord.getString("title")%></td>
			  <td><%=rsSelectRecord.getString("Class_Year")%></td>
			  <td><%=rsSelectRecord.getString("quarter")%></td>
			  <td><%=rsSelectRecord.getString("Grade_Option")%></td>
			  <td><%=rsSelectRecord.getString("Grade_Reveived")%></td>
			  <td><%=rsSelectRecord.getString("Units")%></td>
	    </tr>
	  <%
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
		
		<!-- THis is the gpa of each quarter -------------"select c.Class_Year,c.quarter,sum(e.units) as t,sum(e.units*g.NUMBER_GRADE) as total  --> 
		<div class="panel panel-danger">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Quarter GPA</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
			    <th>#</th> 
				<th>Class Year</th>
				<th>Quarter</th>
				<th>Units</th>
				<th>GPA Of Quarter</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  cnt =1; 
	  while(rsSelectRecord1.next())
	  {
	  %>
	    <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord1.getString("Class_Year")%> </td>
			  <td><%=rsSelectRecord1.getString("quarter")%> </td>
			  <td><%=rsSelectRecord1.getFloat("units")%> </td>
			  <td><%=rsSelectRecord1.getFloat("total")/rsSelectRecord1.getFloat("units")%></td>
	    </tr>
	  <%
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
		
		
		<!-- This is the culmulative gpa of the student  "select sum(e.units) as t,sum(e.units*g.NUMBER_GRADE) as total--> 
		<div class="panel panel-warning">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Cumulative GPA</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Total Units</th>
				<th>Cumulative GPA</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  cnt =1; 
	  while(rsSelectRecord2.next())
	  {
	  %>
	    <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord2.getFloat("units")%></td>
			  <td><%=rsSelectRecord2.getFloat("total")/rsSelectRecord2.getFloat("units")%> </td>
	    </tr>
	  <%
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
		
		
		
		
		</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
  </body>
</html>
<%
    try{
      if(psInsertRecord !=null){ psInsertRecord.close();}
	  if(rsSelectRecord !=null){ rsSelectRecord.close();}
      if(conn!=null){ conn.close();}
    }catch(Exception e){
      e.printStackTrace(); 
    }
%>
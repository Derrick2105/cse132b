<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord1=null;
  PreparedStatement psInsertRecord2=null;
  ResultSet rsSelectRecord1 =null;
  ResultSet rsSelectRecord2 =null;
  String sqlSelectRecord =null;

  
  String sqlInsertRecord=null;
    
  String IDAsString =request.getParameter("ID");
  String Name = request.getParameter("Name");
  String Concentration = null;
  String unitsCon = null;
  
  
  Boolean BS = false;
  Boolean MS = false;
  int ID = 0;
  int unitsC = 0; 
  if (request.getParameter("BS") != null){
	  BS = true;
	  MS = false;
  } else if(request.getParameter("MS") != null){
	  MS = true;
	  BS = false;
	  Concentration = request.getParameter("Concentration");
	  unitsCon = request.getParameter("unitConcentration"); 
	  unitsC = Integer.parseInt(unitsCon); 
	  if (Concentration.length()== 0)
		 Concentration = null;
}
  try {
	// Set any empty strings to null so that sql doesn't accept bad requests
	if (Name.length()==0)
	    Name = null;
		
	ID = Integer.parseInt(IDAsString); 
  } catch(NumberFormatException badSSN){
	  System.out.println("Do something with this!!!");
  }

  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  

        
  try
  {
   
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	   sqlInsertRecord="insert into Degree (ID,Name) values(?,?)";
	   psInsertRecord1=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord1.setInt(1,ID);
	   psInsertRecord1.setString(2,Name);		
	   if (MS){
		   sqlInsertRecord="insert into MS (MS_ID, Concentration, units) values(?,?,?)";
	       psInsertRecord2=conn.prepareStatement(sqlInsertRecord);
	       psInsertRecord2.setInt(1,ID);
		   psInsertRecord2.setString(2,Concentration);
		   psInsertRecord2.setInt(3,unitsC);
	   } else if (BS){
		   sqlInsertRecord="insert into BS_BA (BS_BA_ID) values(?)";
	       psInsertRecord2=conn.prepareStatement(sqlInsertRecord);
	       psInsertRecord2.setInt(1,ID);
	   } else 
		   throw new SQLException();
	   conn.setAutoCommit(false);
	   psInsertRecord1.executeUpdate();
	   psInsertRecord2.executeUpdate();
	   
	   
	   //if (MS){
	   //   sqlSelectRecord ="SELECT m.MS_ID as ID,d.Name as Name,'Masters' as Type, m.Concentration as Concentration FROM MS m,Degree d where m.MS_ID = ? and m.Concentration = ? and m.MS_ID = d.ID";
		//  psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	  //    psInsertRecord1.setInt(1,ID);
		//  psInsertRecord1.setString(2,Concentration);
	  // } else {
		//  sqlSelectRecord ="SELECT BS_BA_ID as ID,d.Name as Name,'Bachelors' as Type, 'N/A' as Concentration FROM BS_BA,Degree where BS_BA_ID = ? and BS_BA_ID = ID";
	//	  psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	  //    psInsertRecord1.setInt(1,ID);
	 //  }
	  // rsSelectRecord1=psInsertRecord1.executeQuery();
	   conn.commit();
	   conn.setAutoCommit(true);
   }
   else if(buttonDec.equals("update")){
	   sqlInsertRecord="update Degree set Name = ? where ID = ?";
	   psInsertRecord1=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord1.setString(1,Name);
	   psInsertRecord1.setInt(2,ID);
	   
	   conn.setAutoCommit(false);
	   psInsertRecord1.executeUpdate();
	   
	   //	if (MS){
	   //   sqlSelectRecord ="SELECT m.MS_ID as ID,d.Name,'Masters' as Type, m.concentration as Concentration FROM MS m,Degree d where m.MS_ID = ? and m.Concentration = ? and m.MS_ID = d.ID";
		//  psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	  //    psInsertRecord1.setInt(1,ID);
		//  psInsertRecord1.setString(2,Concentration);
	  // } else {
		//  sqlSelectRecord ="SELECT BS_BA_ID as ID,Name,'Bachelors' as Type, 'N/A' as Concentration FROM BS_BA,Degree where BS_BA_ID = ? and BS_BA_ID = ID";
	//	  psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	 //    psInsertRecord1.setInt(1,ID);
	 //  }
	 //  rsSelectRecord1=psInsertRecord1.executeQuery();
	   conn.commit();
	   conn.setAutoCommit(true);
   }
   else if(buttonDec.equals("delete")){
	   sqlInsertRecord="Delete from Degree where ID = ?";
	   psInsertRecord1=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord1.setInt(1,ID);
	   psInsertRecord1.executeUpdate();
	   
	   //if (MS){
	   //   sqlSelectRecord ="SELECT m.MS_ID as ID,d.Name Name,'Masters' as Type, m.Concentration as Concentration FROM MS m,Degree d where m.MS_ID = ? and m.Concentration = ? and m.MS_ID = d.ID";
		//  psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	   //   psInsertRecord1.setInt(1,ID);
		//  psInsertRecord1.setString(2,Concentration);
	   //} else {
		//  sqlSelectRecord ="SELECT BS_BA_ID as ID,Name,'Bachelors' as Type, 'N/A' as Concentration FROM BS_BA,Degree where BS_BA_ID = ? and BS_BA_ID = ID";
		//  psInsertRecord1=conn.prepareStatement(sqlSelectRecord);
	   //   psInsertRecord1.setInt(1,ID);
	  // }
	  // rsSelectRecord1=psInsertRecord1.executeQuery();

   }
   
  }
  catch(Exception e)
  {
	  if (conn != null) {
         System.err.print("Transaction is being rolled back");
         conn.rollback();
      }
	  conn.setAutoCommit(true);
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>

			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord1!=null)
      {
       psInsertRecord1.close();
      }
      if(psInsertRecord2!=null)
      {
       psInsertRecord2.close();
      }
	  if(rsSelectRecord1!=null)
      {
        rsSelectRecord1.close();
      }
	  if(rsSelectRecord2!=null)
      {
        rsSelectRecord2.close();
      }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord = null;
  ResultSet rsSelectRecord = null;

  String sqlSelectRecord =null;

  String sqlInsertRecord = null;
        
  String title =request.getParameter("title");
  

  // Set any empty strings to null so that sql doesn't accept bad requests
  if (title.length()==0){ title = null;}
		

  String buttonDec = request.getParameter("Sumbit"); 
       
  try{
   
   if( buttonDec.equals(null)){ 
       System.out.println("buttonDec was nullllll");
   
   }else if(buttonDec.equals("request")){	
	   sqlSelectRecord ="SELECT e.Section_ID,s.First_Name,s.Middle_Initial,s.Last_Name,s.Per_Of_Attendance,s.Residency,s.SSN, s.Enrolled, e.Grade_Option,e.Units, e.Grade_Reveived FROM Enrolled e,Student s, Classes c WHERE e.Student_ID = s.Student_ID and c.Section_ID = e.Section_ID and c.title = ?"
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,title);	
	   rsSelectRecord=psInsertRecord.executeQuery();
	   
	   
   } 
  }catch(Exception e){
	  if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}  
      if(conn!=null){ conn.close();}
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Section Id</th>
				<th>Class Title</th>
				<th>Course Number</th>
				<th>Instructor</th>
				<th>Enrollment Limit</th>
				<th>Units</th>
				<th>Grade Option</th>
				<th>Grade Received</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Section_ID")%> </td>
			  <td><%=rsSelectRecord.getString("title")%> </td>
			  <td><%=rsSelectRecord.getString("Course_offering")%></td>
			  <td><%=rsSelectRecord.getString("Class_Instructor")%></td>
			  <td><%=rsSelectRecord.getString("Enrollment_Limit")%></td>
			  <td><%=rsSelectRecord.getString("Units")%></td>
			  <td><%=rsSelectRecord.getString("Grade_Option")%></td>
			  <td><%=rsSelectRecord.getString("Grade_Reveived")%></td>
			  </tr>
	  <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
  </body>
</html>
<%
    try{
      if(psInsertRecord !=null){ psInsertRecord.close();}
	  if(rsSelectRecord !=null){ rsSelectRecord.close();}
      if(conn!=null){ conn.close();}
    }catch(Exception e){
      e.printStackTrace(); 
    }
%>
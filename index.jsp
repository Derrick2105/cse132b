<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
   
  //This is for the student form 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  sqlSelectRecord ="SELECT * FROM student";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
  
  //this is for courses 
  PreparedStatement psCourseRecord=null;
  ResultSet rsCourseRecord =null;
  String sqlCourseRecord =null;
  sqlCourseRecord ="SELECT * FROM courses";
	   psCourseRecord=conn.prepareStatement(sqlCourseRecord);
	   rsCourseRecord=psCourseRecord.executeQuery();

  //This is for classes 
  PreparedStatement psClassesRecord=null;
  ResultSet rsClassesRecord =null;
  String sqlClassesRecord =null;
  sqlClassesRecord ="SELECT * FROM classes";
	   psClassesRecord=conn.prepareStatement(sqlClassesRecord);
	   rsClassesRecord=psClassesRecord.executeQuery();
  
   //This is for Faculty
  PreparedStatement psFacultyRecord=null;
  ResultSet rsFacultyRecord =null;
  String sqlFacultyRecord =null;
  sqlFacultyRecord ="SELECT * FROM faculty";
	   psFacultyRecord=conn.prepareStatement(sqlFacultyRecord);
	   rsFacultyRecord=psFacultyRecord.executeQuery();
	   
  //This is for probabtion
  PreparedStatement psProbationRecord=null;
  ResultSet rsProbationRecord =null;
  String sqlProbationRecord =null;
  sqlProbationRecord ="SELECT * FROM probation";
	   psProbationRecord=conn.prepareStatement(sqlProbationRecord);
	   rsProbationRecord=psProbationRecord.executeQuery();
  %>
<html lang="en">
  <head>
  <!------------- updated version ---------------------> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>
  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
		    <li role="presentation"><a href="report.jsp">Report</a></li>
            <li role="presentation" class="active"><a href="index.jsp">Form</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>
	  <!-------- Buttons ---------> 
		
		<div class = "row">
		<div class = "btn-group">
		<a class="btn btn-Info btn-lg" href= "report.jsp" >
			<span class="glyphicon glyphicon-file" ></span> Request Report 
		  </a>
		</div>
		<div class = "btn-group" id = "moveRight" align = "right">
		<div class = "btn-group" >
			<button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
			 <span class="glyphicon glyphicon-align-justify" ></span> Forms: A - D <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
			  <li><a href="#" id = "courseGradesAction">Course Grades</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "concentrationAction">Concentration</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "enrollmentAction">Course Enrollment</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "courseAction">Course</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id ="classAction">Class</a></li>
			  <li class="divider"></li>
			   <li><a href="#" id = "ConCorAction">Concentration Course</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "departAction">Department</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "degreeAction">Degree Record</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "DegreesCompletedAction">Degree Completed</a></li>
		  </ul>
		</div>
		<div class = "btn-group" >
			<button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
			 <span class="glyphicon glyphicon-align-justify" ></span> Forms: E - M <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
			  <li><a href="#" id = "facultyAction">Faculty</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "facDeptAction">Faculty Department</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "gradAdvisAction">Graduate Advisor</a></li>
			  <li class="divider"></li>
			  <li><a href="#" id = "majorminorAction">Majors and Minors</a></li>
			  <li class="divider"></li>
			   <li><a href="#" id = "courseUpdate">Old Course Number</a></li>
		  </ul>
		</div>
		<div class="btn-group">
		  <button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
			 <span class="glyphicon glyphicon-align-justify" ></span> Form: P - Z <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
		  <li><a href="#" id = "probationAction">Probation Info</a></li>
		  <li class="divider"></li>
          <li><a href="#" id = "prerequisiteAction">Prerequisites</a></li>
          <li class="divider"></li>
		  <li><a href="#" id = "reviewInfoAction">Review Info</a></li>
		  <li class="divider"></li>
          <li><a href="#" id = "requirementsAction">Requirements</a></li>
		  <li class="divider"></li>
          <li><a href="#" id = "studentAction">Student</a></li>
		  <li class="divider"></li>
		  <li><a href="#" id = "thesisAction">Thesis Committee</a></li>
		  <li class="divider"></li>
          <li><a href="#" id = "weeklyInfoAction">Weekly Info</a></li>
        </ul>
      </div>
	  </div>
	  </div> 
	  <br></br>
	  <br></br>
	  <br></br>
     <div class="jumbotron" id ="hideJumbo">
        <h1>Update, Delete, and Add</h1>
        <p class="lead">The choice is yours once you select your form above.</p>		
      </div>
	  
	<BR></br>
	<BR></br>
	<!-- hide / show forms --> 
	
	 <div class="container">
	<!-- Course Entry Form input column --> 
	<!---- Concentration Course adding ---> 
	<form role="group" action = "ConcentrationCourse.jsp" id ="conCorForm" >
		<h1 align = "center" class="panel-title"><big><b>Concentration association with Course</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Concentration</label>
			<input required = "true" type="text" class="form-control" name = "Concentration" >
		  </div>
		   <div class="form-group">
			<label>Course Number</label>
			<input required = "true" type="text" class="form-control" name ="CourseNum">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!-------------Thesis Forms ------------>
	<form role="group" action = "thesisForm.jsp" id ="thesisForm" >
		<h1 align = "center" class="panel-title"><big><b>Thesis Committee</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Student ID</label>
			<input required = "true" type="text" class="form-control" name = "Student_ID" >
		  </div>
		   <div class="form-group">
			<label>ID</label>
			<input required = "true" type="number" class="form-control" name ="ID">
		  </div>
		  <div class="form-group">
			<label>Faculty: 1</label>
			<input required = "true" type="text" class="form-control" name ="Faculty1">
		  </div>
		  <div class="form-group">
			<label>Faculty: 2</label>
			<input required = "true" type="text" class="form-control" name ="Faculty2">
		  </div>
		  <div class="form-group">
			<label>Faculty: 3</label>
			<input required = "true" type="text" class="form-control" name ="Faculty3">
		  </div>
		  
		  <!-- checkbox --> 
		  <div class="checkbox">
			  <label><input onclick="showHidePHD()" id="checkPHD" name = "PHDCand" type="checkbox" value="y">PhD Candidate</label>
		  </div>
		  <!-- under graduate choices --> 
		  <div id = "fac4">
			   <div class="form-group">
				<label >Faculty: 4</label>
				<input type="text" class="form-control" name ="Faculty4">
			  </div>
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	
	<!--------- Concentration -------------->
	<form role="group" action = "Concentration.jsp" id ="concentrationForm" >
		<h1 align = "center" class="panel-title"><big><b>Concentration</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Concentration</label>
			<input required = "true" type="text" class="form-control" name = "Concentration" >
		  </div>
		   <div class="form-group">
			<label>Course Number</label>
			<input required = "true" type="text" class="form-control" name ="Course_Number">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!------- Requirements --------------->
	<form role="group" action = "Requirements.jsp" id ="requirementsForm" >
		<h1 align = "center" class="panel-title"><big><b>Requirements</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Category ID AKA: C_ID</label>
			<input required = "true" type="number" class="form-control" name = "C_ID" >
		  </div>
		   <div class="form-group">
			<label>Unit Requirement</label>
			<input type="number" class="form-control" name ="Unit_Requirement">
		  </div>
		  <div class="form-group">
			<label>ID</label>
			<input required = "true" type="number" class="form-control" name ="ID">
		  </div>
		  <div class="form-group">
			<label>Department ID</label>
			<input type="number" class="form-control" name ="Department_ID">
		  </div>
		  <div class="form-group">
			<label>Minimum Cumulative GPA</label>
			<input type="text" class="form-control" name ="Min_Cat_GPA">
		  </div>
		  <div class="form-group">
			<label>Category Name</label>
			<input type="text" class="form-control" name ="Category_Name">
		  </div>
		  <div class="form-group">
			<label>Major and Minor</label>
			<input type="text" class="form-control" name ="Major_Minor">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!------- Major and Minor ---------> 
	<form role="group" action = "MajorsAndMinors.jsp" id ="major/minorForm" >
		<h1 align = "center" class="panel-title"><big><b>Majors and Minors</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Undergraduate ID</label>
			<input required = "true" type="text" class="form-control" name = "Undergraduate_ID" >
		  </div>
		   <div class="form-group">
			<label>Category ID AKA C_ID</label>
			<input required = "true" type="number" class="form-control" name ="C_ID">
		  </div>
		  <div class="form-group">
			<label>Status</label>
			<input required = "true" type="text" class="form-control" name ="Status">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!--  Prerequisites --> 
		<form role="group" action = "Prerequisites.jsp" id ="prerequisitesForm" >
		<h1 align = "center" class="panel-title"><big><b>Prerequisites</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Course ID</label>
			<input required = "true" type="text" class="form-control" name = "Course_ID" >
		  </div>
		   <div class="form-group">
			<label>Prerequisites ID</label>
			<input required = "true" type="text" class="form-control" name ="Prereq_ID">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!-- COurse FOrm -------------------------------->
	<form role="group" action = "courseForm.jsp" id ="courseForm" >
		<h1 align = "center" class="panel-title"><big><b>COURSE</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group has-success">
			<label>Course Number</label>
			<input type="text" required = "true" class="form-control" name = "Course" >
		  </div>
		   <div class="form-group has-success">
			<label>Units</label>
			<input type="number" class="form-control" name = "Units">
		  </div>
		   <div class="form-group">
			<label>Consent Required</label>
			<input type="text" class="form-control" name ="Consent" >
		  </div>
		  <div class="form-group">
			<label>Lab Work</label>
			<input type="text" class="form-control" name ="Lab">
		  </div>
		   <div class="form-group has-success">
			<label>Grading Option</label>
			<input type="text" class="form-control" name ="Grading">
		  </div>
		   <div class="form-group has-success">
			<label>Department Number</label>
			<input type="number" class="form-control" name ="Department">
		  </div>
		  <p style = "color:green">*If form green, must be filled out for update and delete</p>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!-- Class entry form input-->
		<form role="group" action = "classForm.jsp" id ="classForm" >
		<h1 align = "center" class="panel-title"><big><b>CLASSES</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group has-success">
			<label>Class Instructor</label>
			<input type="text" class="form-control" name = "Instructor" >
		  </div>
		   <div class="form-group has-success">
			<label>Course Offering</label>
			<input type="text" class="form-control" name = "Offering">
		  </div>
		   <div class="form-group has-success">
			<label>Section ID</label>
			<input required = "true" type="number" class="form-control" name ="Section" >
		  </div>
		  <div class="form-group has-success">
			<label>Title</label>
			<input type="text" class="form-control" name ="Title">
		  </div>
		   <div class="form-group has-success">
			<label>Quarter</label>
			<input type="text" class="form-control" name ="Quarter">
		  </div>
		   <div class="form-group has-success">
			<label>Class Year</label>
			<input type="number" class="form-control" name ="Year">
		  </div>
		  <div class="form-group has-success">
			<label>Enrollment Limit</label>
			<input type="number" class="form-control" name ="Enrollment_Limit">
		  </div>
		  <div class="form-group has-success">
			<label>Start Date</label>
			<input type="text" class="form-control" name ="start" placeholder ="EX: 2003-12-31">
		  </div>
		  <div class="form-group has-success">
			<label>End Date</label>
			<input type="text" class="form-control" name ="end" placeholder = "EX: 2003-12-31">
		  </div>
		  <p style = "color:green">*If form green, must be filled out for update and delete</p>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	
	<!--Faculty--> 
		<form role="group" action = "facultyForm.jsp" id ="facultyForm" >
		<h1 align = "center" class="panel-title"><big><b>FACULTY</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Name</label>
			<input required = "true" type="text" class="form-control" name = "Name" >
		  </div>
		   <div class="form-group">
			<label>Title</label>
			<input type="text" class="form-control" name ="Title">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!-- Course enrollment --> 
		<form role="group" action = "enrollmentForm.jsp" id ="enrollmentForm" >
		<h1 align = "center" class="panel-title"><big><b>ENROLLMENT</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Student ID</label>
			<input required = "true" type="text" class="form-control" name = "Student_ID" >
		  </div>
		   <div class="form-group">
			<label>Section ID</label>
			<input required = "true" type="number" class="form-control" name = "Section">
		  </div>
		   <div class="form-group">
			<label>Grade option </label>
			<input type="text" class="form-control" name ="GradeO" >
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	    </form>
		
	<!-- Classes taken in past --> 
		<form role="group" action = "takenForm.jsp" id ="takenForm" >
		<h1 align = "center" class="panel-title"><big><b>CLASS TAKEN</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Student ID</label>
			<input type="text" class="form-control" name = "Student_ID" >
		  </div>
		   <div class="form-group">
			<label>Course</label>
			<input type="number" class="form-control" name = "Course">
		  </div>
		   <div class="form-group">
			<label>Quarter</label>
			<input type="text" class="form-control" name ="Quarter" >
		  </div>
		  <div class="form-group">
			<label>Grade</label>
			<input type="text" class="form-control" name ="Grade">
		  </div>
		  <div class="form-group">
			<label>Year</label>
			<input type="text" class="form-control" name ="Year">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	    </form>
		

	
	<!-- Probation info submission--> 
		<form role="group" action = "probationForm.jsp" id ="probationForm" >
		<h1 align = "center" class="panel-title"><big><b>PROBATION</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Student ID</label>
			<input type="text" class="form-control" name = "Student_ID" >
		  </div>
		   <div class="form-group">
			<label>Reason</label>
			<input type="text" class="form-control" name = "Reason">
		  </div>
		   <div class="form-group">
			<label>Start Quarter</label>
			<input type="text" class="form-control" name ="Start_Quarter" >
		  </div>
		  <div class="form-group">
			<label>Start Year</label>
			<input type="text" class="form-control" name ="Start_Year">
		  </div>
		  <div class="form-group">
			<label>End Quarter</label>
			<input type="text" class="form-control" name ="End_Quarter">
		  </div>
		  <div class="form-group">
			<label>End Year</label>
			<input type="text" class="form-control" name ="End_Year">
		  </div> 
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	    </form>	
		

	
	<!-- Degree Requirements info submission --> 
	<form role="group" action = "degreeRecord.jsp" id ="degreeForm" >
	<h1 align = "center" class="panel-title"><big><b>DEGREE REQUIREMENTS</b></big></h1>
	<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>ID</label>
			<input type="number" required = "true" class="form-control" name ="ID">
		  </div>
		  <div class="form-group">
			<label>Name</label>
			<input type="text" class="form-control" name ="Name">
		  </div>
		  <!-- check boxes --> 
		   <div class="checkbox">
			  <label><input onchange="showHideMS()"  id ="msCheck" name ="MS" type="checkbox" value="y">Master</label>
	      </div>
		  <div class="checkbox">
			  <label><input  name = "BS" type="checkbox" value="y">Bachelors</label>
		  </div>
		  <!-- under graduate choices --> 
		  <div id = "msForm">
			   <div class="form-group">
				<label >Concentration</label>
				<input type="text" class="form-control" id = "msReq" name ="Concentration">
			  </div>
			  <div class="form-group">
				<label >Unit Requirement for Concentration</label>
				<input type="number" class="form-control" id = "msUnit" name ="unitConcentration">
			  </div>
		  </div> 
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!-- Course Number Update Form--> 
	<form role="group" action = "UpdateCourseNumForm.jsp" id ="CourseNumUpdateForm" >
	<h1 align = "center" class="panel-title"><big><b>Old Course Number</b></big></h1>
	<BR></br>
		<BR></br>
		  <div class="form-group">
		  <div class="form-group">
			<label >Course Number</label>
			<input required="true"  type="text" class="form-control" name ="oldNum">
		  </div>
			<label>Old Course Number</label>
			<input required ="true" type="text" class="form-control" name = "newNum" >
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button> 
	</form>
	
	
	
	<!-- student form input column --> 
	<form role="group" action = "studentRecord.jsp" id ="studentForm" >
		<h1 align = "center" class="panel-title"><big><b>STUDENT ENTRY</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group has-success">
			<label>Student ID</label>
			<input required = "true" type="text" class="form-control" name = "Student_ID" placeholder = "EX: a10003333">
		  </div>
		   <div class="form-group has-success">
			<label> First Name</label>
			<input type="text" class="form-control" name = "First_Name">
		  </div>
		   <div class="form-group">
			<label>Middle Initial </label>
			<input type="text" class="form-control" name ="Middle_Initial" maxlength= "1" placeholder = "A for Alvarado">
		  </div>
		  <div class="form-group has-success">
			<label>Last Name</label>
			<input type="text" class="form-control" name ="Last_Name">
		  </div>
		   <div class="form-group">
			<label>SSN</label>
			<input type="text" class="form-control" name ="SSN">
		  </div>
		   <div class="form-group has-success">
			<label> Enrolled</label>
			<input type="text" class="form-control" name ="Enrolled" maxlength= "1" placeholder = "Y for YES or N for NO">
		  </div>
		   <div class="form-group">
			<label >Period of Attendance</label>
			<input type="text" class="form-control" name ="Period_Of_Attendance">
		  </div>
		  <div class="form-group has-success">
			<label>Residency</label>
			<input type="text" class="form-control" name = "Residency">
		  </div>
		  <h4>Student Status: check one below</h4>
		  <div class="checkbox">
			  <label><input onchange="showHideUnder()"  id ="underCheck" name ="undergrad" type="checkbox" value="y">Undergraduate</label>
	      </div>
		  <div class="checkbox">
			  <label><input onchange="showHideGrad()" id = "gradCheck" name = "grad" type="checkbox" value="y">Graduate</label>
		  </div>
		  <!-- under graduate choices --> 
		  <div id = "underForm">
			  <!--check boxes for ms/bs --> 
			  <h4>Check if MS/BS Student</h4>
			  <div class="checkbox">
				  <label><input name = "ms/bs" type="checkbox" value="y">MS/BS</label>
			  </div>
			   <div class="form-group">
				<label >College</label>
				<input type="text" class="form-control" name ="college">
			  </div>
		  </div> 
		  <!-- graduate choices --> 
		  <div id = "gradForm">
			  <div class="form-group">
				<label >Type of Graduate</label>
				<input type="text" class="form-control" name ="type_of_graduate">
			  </div>
			  <div class="form-group has-success">
				<label>Department ID</label>
				<input type="number" class="form-control" name = "department_id">
			  </div>
		  </div> 
		   <p style = "color:green">*If form green, must be filled out for update and delete</p>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>
	</form>
	
	<!------------------ Graduate Advisor ---------------------------------> 
	<form role="group" action = "GraduateAdvisor.jsp" id ="gradAdvisForm" >
	<h1 align = "center" class="panel-title"><big><b>Graduate Advisor</b></big></h1>
		<br></br>
		<br></br>
		  <div class="form-group">
			<label>Faculty Name</label>
			<input type="text" class="form-control" name ="Faculty_Name">
		  </div>
		  <div class="form-group">
			<label>Graduate ID</label>
			<input required="true" type="text" class="form-control" name = "Graduate_ID" >
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>		  
	</form>
	
	<!------------------FACULTY Department --------------------------------> 
	<form role="group" action = "FacultyDepartment.jsp" id ="facDeptForm" >
	<h1 align = "center" class="panel-title"><big><b>Faculty Department</b></big></h1>
		<br></br>
		<br></br>
		  <div class="form-group">
			<label>Faculty Name</label>
			<input required = "true" type="text" class="form-control" name ="Faculty_Name">
		  </div>
		  <div class="form-group">
			<label> Department Number </label>
			<input type="number" class="form-control" name = "Department_Num" >
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>		  
	</form>
	
	
	<!----------------------- Department Form -----------------------------> 
	<form role="group" action = "DepartmentForm.jsp" id ="deptForm" >
	<h1 align = "center" class="panel-title"><big><b>Department</b></big></h1>
	<br></br>
	<br></br>
		  <div class="form-group">
			<label>Department Name</label>
			<input type="text" class="form-control" name ="Department_Name">
		  </div>
		  <div class="form-group">
			<label> Department ID </label>
			<input required = "true" type="number" class="form-control" name = "Department_ID" >
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>		  
	</form>
	

	
	<!---------------- Course Grades Form -------Might need to make required-------------------------------> 
	<form role="group" action = "CourseGrades.jsp" id ="courseGradesForm" >
	<h1 align = "center" class="panel-title"><big><b>Course Grades</b></big></h1>
		<br></br>
		<br></br>
		  <div class="form-group">
			<label>Section ID</label>
			<input type="number" class="form-control" name ="Section_ID">
		  </div>
		  <div class="form-group">
			<label>Student ID</label>
			<input type="text" class="form-control" name = "Student_ID" >
		  </div>
		  <div class="form-group">
			<label>Grade Received</label>
			<input type="text" class="form-control" name ="Grade_Received">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>		  
	</form>
	
	<!-----------------Review Info --------------------------------------------> 
	<form role="group" action = "ReviewInfo.jsp" id ="reviewInfoForm" >
	<h1 align = "center" class="panel-title"><big><b>Review Info</b></big></h1>
		<br></br>
		<br></br>
		  <div class="form-group">
			<label>Section ID</label>
			<input required = "true" type="number" class="form-control" name ="Section_ID">
		  </div>
		  <div class="form-group">
			<label>Review ID</label>
			<input required ="true" type="number" class="form-control" name = "Review_ID" >
		  </div>
		  <div class="form-group">
			<label>Days</label>
			<input required= "true" type="text" class="form-control" name ="Days">
		  </div>
		  <div class="form-group">
			<label>Location</label>
			<input type="text" class="form-control" name ="Location">
		  </div>
		  <div class="form-group">
			<label>Meeting Time</label>
			<input required= "true" type="number" class="form-control" name ="Meeting_time">
		  </div>
		  <div class="form-group">
			<label>Review Day</label>
			<input type="text" class="form-control" name ="reviewDay" placeholder = "EX: 2003-12-31">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>		  
	</form>
	
	<!---------------- Student Degrees ------------------------------------------>
	<form role="group" action = "DegreesCompleted.jsp" id ="DegreesCompletedForm" >
	<h1 align = "center" class="panel-title"><big><b>Degrees Completed</b></big></h1>
		<br></br>
		<br></br>
		  <div class="form-group">
			<label>Student ID</label>
			<input required = "true" type="text" class="form-control" name ="Student_ID">
		  </div>
		  <div class="form-group">
			<label>Degree Type</label>
			<input required ="true" type="text" class="form-control" name = "Degree_Type" >
		  </div>
		  <div class="form-group">
			<label>Degree Name</label>
			<input required= "true" type="text" class="form-control" name ="Degree_Name">
		  </div>
		  <div class="form-group">
			<label>Degree School</label>
			<input required= "true" type="text" class="form-control" name ="Degree_School">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>		  
	</form>
	
	<!--------------Weekly Info ------------------------------------------------->
	<form role="group" action = "WeeklyInfo.jsp" id ="weeklyInfoForm" >
	<h1 align = "center" class="panel-title"><big><b>Weekly Info</b></big></h1>
		<br></br>
		<br></br>
		  <div class="form-group">
			<label>Section ID</label>
			<input required = "true" type="number" class="form-control" name ="Section_ID">
		  </div>
		  <div class="form-group">
			<label>Meeting ID</label>
			<input required ="true" type="number" class="form-control" name = "Review_ID" >
		  </div>
		  <div class="form-group">
			<label>Days</label>
			<input required= "true" type="text" class="form-control" name ="Days">
		  </div>
		  <div class="form-group">
			<label>Location</label>
			<input type="text" class="form-control" name ="Location">
		  </div>
		  <div class="form-group">
			<label>Meeting Time: Start</label>
			<input required= "true" type="text" class="form-control" name ="Meeting_timeStart">
		  </div>
		  <div class="form-group">
			<label>Meeting Time: End</label>
			<input required= "true" type="text" class="form-control" name ="Meeting_timeEnd">
		  </div>
		  <div class="form-group">
			<label>Mandatory</label>
			<input type="text" class="form-control" name ="Mandatory">
		  </div>
		  <div class="form-group">
			<label>Type</label>
			<input required= "true" type="text" class="form-control" name ="type">
		  </div>
		  <br></br>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="add">Submit query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update query</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="delete">Delete query</button>		  
	</form>
	</div>
	
	
   <br>
   <br>
	<div class="panel panel-success" id ="probationTable">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Probation</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>Reason</th>
				<th>Start Quarter</th>
				<th>Start Year</th>
				<th>End Quarter</th>
				<th>End Year</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int proCnt=1;
	  while(rsProbationRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=proCnt%></th>
			  <td><%=rsProbationRecord.getString("Student_ID")%> </td>
			  <td><%=rsProbationRecord.getString("Reason")%> </td>
			  <td><%=rsProbationRecord.getString("period_start_quarter")%> </td>
			  <td><%=rsProbationRecord.getInt("period_start_year")%></td>
			  <td><%=rsProbationRecord.getString("period_end_quarter")%></td>
			  <td><%=rsProbationRecord.getInt("period_end_year")%></td>


			  </tr>
			   <%
	   proCnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
	<!------------------------ Faculty table display -------------------------------> 
   <br>
   <br>
	<div class="panel panel-success" id = "facultyTable">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Name</th>
				<th>Title</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int facultyCnt=1;
	  while(rsFacultyRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=facultyCnt%></th>
			  <td><%=rsFacultyRecord.getString("Name")%> </td>
			  <td><%=rsFacultyRecord.getString("Title")%> </td>


			  </tr>
			   <%
	   facultyCnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	
	<!------------------------ class table display box ----------------------------->
   <br>
   <br>
	<div class="panel panel-success" id = "classTable">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Class</h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Class Instructor</th>
				<th>Course Offering</th>
				<th>Section</th>
				<th>Title</th>
				<th>Quarter</th>
				<th>Year</th>
				<th>Enrollment Limit</th> 
			  </tr>
			</thead>
			<tbody>
	<%
	  int classCnt=1;
	  while(rsClassesRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=classCnt%></th>
			  <td><%=rsClassesRecord.getString("Class_Instructor")%> </td>
			  <td><%=rsClassesRecord.getString("Course_Offering")%> </td>
			  <td><%=rsClassesRecord.getString("Section_ID")%> </td>
			  <td><%=rsClassesRecord.getString("title")%></td>
			  <td><%=rsClassesRecord.getString("quarter")%></td>
			  <td><%=rsClassesRecord.getString("Class_Year")%></td>
			  <td><%=rsClassesRecord.getString("Enrollment_Limit")%></td>
		</tr>
			   <%
	   classCnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	
	<!------------------------ Course table display box ---------------------------->
   <br>
   <br>
	<div class="panel panel-success" id= "courseTable">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Course </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Course Number</th>
				<th>Units</th>
				<th>Consent Required</th>
				<th>Lab Work</th>
				<th>Grading Option</th>
				<th>Department Number</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int cntCourse=1;
	  while(rsCourseRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cntCourse%></th>
			  <td><%=rsCourseRecord.getString("Course_Number")%> </td>
			  <td><%=rsCourseRecord.getString("Units")%> </td>
			  <td><%=rsCourseRecord.getString("Consent_Required")%> </td>
			  <td><%=rsCourseRecord.getString("Lab_Work")%></td>
			  <td><%=rsCourseRecord.getString("Grading_Option")%></td>
			  <td><%=rsCourseRecord.getString("Department_Num")%></td>

			  </tr>
			   <%
	   cntCourse++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
	
	<br>
	<br>
	<!------------------------ STUDENT TABLE DISPLAY BOX --------------------------> 
	<div class="panel panel-success" id = "studentTable">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Student</h3>
   	 </div>

	<div class="table-responsive" id ="scrollFix">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>First Name</th>
				<th>Middle initial</th>
				<th>Last Name</th>
				<th>SSN</th>
				<th>Enrolled</th>
				<th>Period of Attendance</th>
				<th>Residency</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Student_ID")%> </td>
			  <td><%=rsSelectRecord.getString("First_Name")%> </td>
			  <td><%=rsSelectRecord.getString("Middle_Initial")%> </td>
			  <td><%=rsSelectRecord.getString("Last_Name")%></td>
			  <td><%=rsSelectRecord.getString("SSN")%></td>
			  <td><%=rsSelectRecord.getString("Enrolled")%></td>
			  <td><%=rsSelectRecord.getString("Per_Of_Attendance")%></td>
			  <td><%=rsSelectRecord.getString("Residency")%></td>

			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
		
		</div> <!-- container wrapped around tables --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
    </div> <!-- /container -->


    <script>

	var ids = ["studentForm", "degreeForm","probationForm", "thesisForm",
			   "underForm", "takenForm", "enrollmentForm", "facultyForm", "classForm",
			   "courseForm", "studentTable","hideJumbo","gradForm", "CourseNumUpdateForm"
			   ,"courseTable", "deptForm", "facDeptForm", "gradAdvisForm"
			   ,"courseGradesForm", "reviewInfoForm", "weeklyInfoForm"
			   ,"classTable", "facultyTable","probationTable", "DegreesCompletedForm", "prerequisitesForm"
			   ,"major/minorForm", "requirementsForm","msForm", "concentrationForm", "fac4", "conCorForm"];
			   
	var i;
	for(i =0; i< ids.length; i++){
		if(ids[i] != "hideJumbo")
		document.getElementById(ids[i]).style.display = 'none'; 
	}

	
	//SHOWHIDEPHD
	function showHidePHD(){
	if($('#checkPHD').is(":checked")){   
        $("#fac4").show();
		}
    else
        $("#fac4").hide();
	}
	
	//Degree record master checkbox
	function showHideMS(){
	if($('#msCheck').is(":checked")){   
        $("#msForm").show();
		}
    else
        $("#msForm").hide();
	}
	
	//this is gonna either display or hide information for student depending on choice 
	//undergrad
	function showHideUnder(){
	if($('#underCheck').is(":checked"))   
        $("#underForm").show();
    else
        $("#underForm").hide();
	}
	
	function showHideGrad(){
	if($('#gradCheck').is(":checked"))   
        $("#gradForm").show();
    else
        $("#gradForm").hide();
	}
	//ConCorAction
	jQuery("#ConCorAction").click(function(e){
		if(document.getElementById('conCorForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('conCorForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//Concentration
	jQuery("#concentrationAction").click(function(e){
		if(document.getElementById('concentrationForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('concentrationForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//requirements
	jQuery("#requirementsAction").click(function(e){
		if(document.getElementById('requirementsForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('requirementsForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
		

	//Major and Minor 
	jQuery("#majorminorAction").click(function(e){
		if(document.getElementById('major/minorForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('major/minorForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	//prerequisiteAction
	jQuery("#prerequisiteAction").click(function(e){
		if(document.getElementById('prerequisitesForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('prerequisitesForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//DegreesCompletedAction
	jQuery("#DegreesCompletedAction").click(function(e){
		if(document.getElementById('DegreesCompletedForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('DegreesCompletedForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	//Course Grades 
	jQuery("#courseGradesAction").click(function(e){
		if(document.getElementById('courseGradesForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('courseGradesForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//Review Info Action 
	jQuery("#reviewInfoAction").click(function(e){
		if(document.getElementById('reviewInfoForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('reviewInfoForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//Weekly Info 
	jQuery("#weeklyInfoAction").click(function(e){
		if(document.getElementById('weeklyInfoForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('weeklyInfoForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	//Department Form 
	jQuery("#departAction").click(function(e){
	//do something
		if(document.getElementById('deptForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('deptForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//Faculty Department 
	jQuery("#facDeptAction").click(function(e){
	//do something
		if(document.getElementById('facDeptForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('facDeptForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//Graduate Advisor Form 
	jQuery("#gradAdvisAction").click(function(e){
	//do something
		if(document.getElementById('gradAdvisForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('gradAdvisForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	//courseUpdate TODO
	jQuery("#courseUpdate").click(function(e){
	//do something
		if(document.getElementById('CourseNumUpdateForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('CourseNumUpdateForm').style.display='';
			//document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//takes care of showing / make forms disappear 
	jQuery("#studentAction").click(function(e){
	//do something
		if(document.getElementById('studentForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('studentForm').style.display='';
			document.getElementById('studentTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	jQuery("#courseAction").click(function(e){
	//do something
		if(document.getElementById('courseForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('courseForm').style.display='';
			document.getElementById('courseTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		
		e.preventDefault();
	});
	
	jQuery("#classAction").click(function(e){
	//do something
		if(document.getElementById('classForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('classForm').style.display='';
			document.getElementById('classTable').style.display='';
			
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
			
		e.preventDefault();
	});
	
	jQuery("#facultyAction").click(function(e){
	//do something
		if(document.getElementById('facultyForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('facultyForm').style.display='';
			document.getElementById('facultyTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	jQuery("#enrollmentAction").click(function(e){
	//do something
		if(document.getElementById('enrollmentForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('enrollmentForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	jQuery("#takenAction").click(function(e){
	//do something
		if(document.getElementById('takenForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('takenForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	jQuery("#thesisAction").click(function(e){
	//do something
		if(document.getElementById('thesisForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('thesisForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	jQuery("#probationAction").click(function(e){
	//do something
		if(document.getElementById('probationForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('probationForm').style.display='';
			document.getElementById('probationTable').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	jQuery("#degreeAction").click(function(e){
	//do something
		if(document.getElementById('degreeForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('degreeForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	</script>
  </body>
</html>

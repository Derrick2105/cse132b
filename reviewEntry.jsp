<%@ page language="java" import="java.util.*,java.sql.*, java.util.Properties,java.io.*,java.lang.*,java.text.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
  Statement stmt = conn.createStatement();
  PreparedStatement psInsertRecord = null;
  ResultSet rsSelectRecord = null;

  String sqlSelectRecord =null;

  String sqlInsertRecord = null;
      
  String startT = "8:00";
  String endT = "21:00";
  String curDate = null;
  String ID =request.getParameter("ID");
  String startDate =request.getParameter("start");
  String endDate =request.getParameter("end");

  // Set any empty strings to null so that sql doesn't accept bad requests
  if (ID.length()==0){ ID = null;}
  if (startDate.length()==0){ startDate = null;}
  if (endDate.length()==0){ endDate = null;}  
  
  int id = 0;
  try {		
	id = Integer.parseInt(ID);
  } catch(NumberFormatException badSSN){
	  System.out.println("Do something with this!!!");
  }
  String buttonDec = request.getParameter("Submit");      
  try{  
   if( buttonDec.equals(null)){ 
   
   }else if(buttonDec.equals("request")){
       stmt.executeUpdate("create table tempT(day varchar(15), date varchar(15),startTime varchar(15),endTime varchar(15), primary key (day,date,startTime,endTime));");
	   stmt.executeUpdate("create table tempT2(day varchar(15), date varchar(15),startTime varchar(15),endTime varchar(15), primary key (day,date,startTime,endTime));");
       /*curDate is sest to startDate */
	   curDate = startDate;
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
       Calendar c = Calendar.getInstance(); 
	   c.setTime(sdf.parse(endDate));
	   endDate = sdf.format(c.getTime());   
       do {
		     sqlSelectRecord = "select distinct f.Section_ID " +
                               "from enrolled e, enrolled f, classes x " +
                               "where e.Student_ID = f.Student_ID and e.Section_ID = ? and f.Section_ID = x.Section_ID and x.quarter = 'SPRING' and x.Class_Year = 2009";
			 psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	         psInsertRecord.setInt(1,id);
			 rsSelectRecord= psInsertRecord.executeQuery();
		     while (rsSelectRecord.next()){
			   id = rsSelectRecord.getInt("Section_ID");
			   System.out.println("ID: " + id);
	           sqlSelectRecord ="insert into tempT " +
	                    "Select distinct t.day,? as date,r.startTime,r.endTime " +
                        "From (select m.startTime,m.endTime,m.ID,t.day  " +
                              "from meetings m, (select DATENAME(dw,?) as day) t " +
                              "where m.Meeting_Days <> t.day) t, reviewTimes r, meetings m, ClassMeetings c " +
                        "where r.startTime >= ? and r.startTime < ? and c.Section_ID = ? and c.Meet_ID = t.ID and r.startTime not in ( " +
                           "select distinct n.startTime " +
                           "from Meetings m, reviewTimes n, (select DATENAME(dw,?) as day) t,ClassMeetings c " +
                           "where((m.startTime <=n.startTime and n.startTime < m.endTime) or (n.startTime <= m.startTime and m.startTime < n.endTime)) and t.day = m.Meeting_Days " +
	                       "and c.Section_ID = ? and c.Meet_ID = m.ID) " +
						"except select * from tempT";

	           psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	           psInsertRecord.setString(1,curDate);
	           psInsertRecord.setString(2,curDate);
	           psInsertRecord.setString(3,startT);
	           psInsertRecord.setString(4,endT);
	           psInsertRecord.setInt(5,id);
	           psInsertRecord.setString(6,curDate);
	           psInsertRecord.setInt(7,id);
	           psInsertRecord.executeUpdate();
			   sqlSelectRecord ="insert tempT2 " +
			   "Select t.day,? as date,r.startTime,r.endTime " +
                        "From (select m.startTime,m.endTime,m.ID,t.day  " +
                              "from meetings m, (select DATENAME(dw,?) as day) t " +
                              "where m.Meeting_Days <> t.day) t, reviewTimes r, meetings m, ClassMeetings c " +
                        "where r.startTime >= ? and r.startTime < ? and c.Section_ID = ? and c.Meet_ID = t.ID and r.startTime in ( " +
                           "select distinct n.startTime " +
                           "from Meetings m, reviewTimes n, (select DATENAME(dw,?) as day) t,ClassMeetings c " +
                           "where((m.startTime <=n.startTime and n.startTime < m.endTime) or (n.startTime <= m.startTime and m.startTime < n.endTime)) and t.day = m.Meeting_Days " +
	                       "and c.Section_ID = ? and c.Meet_ID = m.ID) " +
						"except select * from tempT2";
				psInsertRecord=conn.prepareStatement(sqlSelectRecord);
				psInsertRecord.setString(1,curDate);
	           psInsertRecord.setString(2,curDate);
	           psInsertRecord.setString(3,startT);
	           psInsertRecord.setString(4,endT);
	           psInsertRecord.setInt(5,id);
	           psInsertRecord.setString(6,curDate);
	           psInsertRecord.setInt(7,id);
				psInsertRecord.executeUpdate();
	       }
	       /* update the curDate */
	       c.setTime(sdf.parse(curDate));
           c.add(Calendar.DATE, 1);  // number of days to add
           curDate = sdf.format(c.getTime());
	   } while (!curDate.equals(endDate));
	   rsSelectRecord= conn.prepareStatement("select * from tempT except select * from tempT2 order by date").executeQuery();
	   stmt.executeUpdate("Drop table tempT");
	   stmt.executeUpdate("Drop table tempT2");
   } 
  }catch(Exception e){
	  
	  if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}  
      if(conn!=null){ stmt.executeUpdate("Drop table tempT");stmt.executeUpdate("Drop table tempT2");conn.close();}
	  System.out.println();
	  System.out.println();
	  System.out.println();
	  System.out.println();
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Day</th>
				<th>Date</th>
				<th>start Time</th>
				<th>end Time</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while( rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("day")%> </td>
			  <td><%=rsSelectRecord.getString("date")%> </td>
			  <td><%=rsSelectRecord.getString("startTime")%> </td>
			  <td><%=rsSelectRecord.getString("endTime")%></td>
			  </tr>
	  <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
  </body>
</html>
<%
    try{
      if(psInsertRecord !=null){ psInsertRecord.close();}
	  if(rsSelectRecord !=null){ rsSelectRecord.close();}
      if(conn!=null){ conn.close();}
    }catch(Exception e){
      e.printStackTrace(); 
    }
%>
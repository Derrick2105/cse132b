<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord = null;
  ResultSet rsSelectRecord = null;
  String sqlSelectRecord =null;
  String sqlInsertRecord = null;
  String degreeName = request.getParameter("degreeName");
  
  PreparedStatement psInsertBSRecord = null; 
  ResultSet rsSelectBSRecord = null; 
  String sqlSelectBSRecord = null; 
  String sqlInsertBSRecord = null; 
  String pid =request.getParameter("PID");
  
  //part 3
  PreparedStatement psInsertRemRecord = null; 
  ResultSet rsSelectRemRecord = null; 
  String sqlSelectRemRecord = null; 
  String sqlInsertRemRecord = null; 
  

  // Set any empty strings to null so that sql doesn't accept bad requests
  if (pid.length()!=9){ pid = null;}
		

  String buttonDec = request.getParameter("Submit"); 
       
  try{
   
   if( buttonDec.equals(null)){ 
       System.out.println("buttonDec was nullllll");
   
   }else if(buttonDec.equals("request")){	
	   sqlSelectBSRecord ="select f.Degree AS Degree,f.sum as Units,case " +
	                        "when sum(f.sum - e.sum) < 0 then 0 " +
	                        "when sum(f.sum - e.sum) >= 0 then sum(f.sum - e.sum) " +
                          "end as sum " +
                          "from (select sum(units) as Sum " +
		                        "from enrolled " +
		                        "where Student_ID = ?) e, " +
		                       "(select d.Name as Degree,sum(r.unit_requirement) as sum " +
		                        "from Requirements r, degree d " +
		                        "where r.id = d.id AND d.Name = ? " +
		                        "group by d.Name,r.Unit_Requirement) f " +
                          "group by f.Degree, f.sum";
	   psInsertBSRecord = conn.prepareStatement(sqlSelectBSRecord); 
	   psInsertBSRecord.setString(1,pid);
	   psInsertBSRecord.setString(2,degreeName); 
	   rsSelectBSRecord=psInsertBSRecord.executeQuery(); 
	   
	   sqlSelectRemRecord = "Select x.C_ID, r.Category_Name, case when (r.Unit_Requirement - sum(x.units)) < 0 then 0 "+ 
							"when (r.Unit_Requirement - sum(x.units)) >= 0 then (r.Unit_Requirement - sum(x.units)) "+
							"end as Units "+
							"From Requirements r,degree d,( "+
							   "Select x.C_ID, x.Course_Num, e.units "+
							   "From Classes f, enrolled e, RequiredCourses x "+  
							   "where  f.Course_offering =  x.Course_Num and e.Section_ID = f.Section_ID and e.student_ID = ? "+
							   "group by x.C_ID, x.Course_Num, e.units) as x "+
							"where d.Name = ? and r.ID=d.ID and r.C_ID = x.C_ID "+
							"group by x.C_ID,r.Unit_Requirement,r.Category_Name"; 
		psInsertRemRecord = conn.prepareStatement(sqlSelectRemRecord);
		psInsertRemRecord.setString(1,pid);
		psInsertRemRecord.setString(2,degreeName);
		rsSelectRemRecord = psInsertRemRecord.executeQuery(); 
	
	   
   } 
  }catch(Exception e){
	  if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}
	  if(psInsertBSRecord!=null){psInsertBSRecord.close();}
	  if(rsSelectBSRecord!=null){rsSelectBSRecord.close();}
	  if(psInsertRemRecord!=null){psInsertRemRecord.close();}
	  if(rsSelectRemRecord!=null){rsSelectRemRecord.close();}
      if(conn!=null){ conn.close();}
	  e.printStackTrace();
      response.sendRedirect("report.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="report.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br></br> 
   <br></br>
   <br></br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Minimum units for Degree </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Degree Name</th>
				<th>Minimum Units</th>
				<th>Remaining Units</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectBSRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectBSRecord.getString("Degree")%> </td>
			  <td><%=rsSelectBSRecord.getString("Units")%> </td>
			  <td><%=rsSelectBSRecord.getString("sum")%> </td>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
	   </tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
	
	<div class="panel panel-primary">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Category Name with units needed for completion </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Category ID</th>
				<th>Category Name</th>
				<th>Units Left</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  cnt=1;
	  while(rsSelectRemRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRemRecord.getString("C_ID")%> </td>
			  <td><%=rsSelectRemRecord.getString("Category_Name")%> </td>
			  <td><%=rsSelectRemRecord.getString("Units")%> </td>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
		</tr>
			</tbody>
		  </table>
		  
		</div>
		</div>
	
		
		
	</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
  </body>
</html>
<%
    try{
      if(psInsertRecord!=null){ psInsertRecord.close();}
	  if(rsSelectRecord!=null){ rsSelectRecord.close();}
      if(conn!=null){ conn.close();}
    }catch(Exception e){
      e.printStackTrace(); 
    }
%>
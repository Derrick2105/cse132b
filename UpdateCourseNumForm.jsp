<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%
  Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
        
  String CourseNumber =request.getParameter("oldNum");
  String NewCourseNumber = request.getParameter("newNum");
  // Set any empty strings to null so that sql doesn't accept bad requests
  if (CourseNumber.length()==0)
	CourseNumber = null;		
  if (NewCourseNumber.length() == 0)
    NewCourseNumber = null;		


  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
      System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("update")){
	   conn.setAutoCommit(false);
	   //sqlInsertRecord="update Courses set Course_Number = ? where Course_Number = ?";
	   //psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   //psInsertRecord.setString(2,CourseNumber);
	   //psInsertRecord.setString(1,NewCourseNumber);
	   //psInsertRecord.executeUpdate();
	   
	   sqlInsertRecord="Insert into OldCoursesNumbers(CourseNum,OldCourseNum) values (?,?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,CourseNumber);
	   psInsertRecord.setString(2,NewCourseNumber);
	   psInsertRecord.executeUpdate();
	   conn.commit();
	   conn.setAutoCommit(true);
	   sqlSelectRecord ="SELECT * FROM Courses";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }  
  }
  catch(Exception e)
  {
	  if (conn != null) {
         System.err.print("Transaction is being rolled back");
         conn.rollback();
      }
	  conn.setAutoCommit(true);
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Course Number</th>
				<th>Units</th>
				<th>Consent Required</th>
				<th>Lab Work</th>
				<th>Grading Option</th>
				<th>Department Number</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Course_Number")%> </td>
			  <td><%=rsSelectRecord.getString("Units")%> </td>
			  <td><%=rsSelectRecord.getString("Consent_Required")%> </td>
			  <td><%=rsSelectRecord.getString("Lab_Work")%></td>
			  <td><%=rsSelectRecord.getString("Grading_Option")%></td>
			  <td><%=rsSelectRecord.getString("Department_Num")%></td>

			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
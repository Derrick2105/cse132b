<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
   
  //This is for the student form where they are enroolled in the current quarter. Need to change
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  ResultSet rsSelectRecord1 =null;
  String sqlSelectRecord =null;
  sqlSelectRecord =
		"Select Distinct(s.Student_ID), s.First_Name, s.Middle_Initial, s.Last_Name, s.SSN, c.quarter, c.Class_Year "+
		"From classes c, enrolled e, student s "+
		"Where c.quarter = 'SPRING' AND c.Class_Year = '2009' "+
		"and e.Section_ID = c.Section_ID and s.Enrolled = 'y'";
  psInsertRecord=conn.prepareStatement(sqlSelectRecord);
  rsSelectRecord=psInsertRecord.executeQuery();
  psInsertRecord=conn.prepareStatement(sqlSelectRecord);
  rsSelectRecord1 = psInsertRecord.executeQuery();
	   
//sections given in current quarter. 
  PreparedStatement psInsertAssistRecord = null; 
  ResultSet rsSelectAssistRecord = null;
  String sqlSelectAssistRecord = null; 
  sqlSelectAssistRecord = 
						"Select * "+
						"From classes c "+ 
						"Where c.quarter = 'SPRING' AND c.Class_Year = '2009' "; 
  psInsertAssistRecord = conn.prepareStatement(sqlSelectAssistRecord);
  rsSelectAssistRecord = psInsertAssistRecord.executeQuery();   
	   
	   
//classes form 
  PreparedStatement psRoasterRecord=null;
  ResultSet rsRoasterRecord =null;
  String sqlRoasterRecord =null;
  sqlRoasterRecord ="SELECT * FROM Classes";
	   psRoasterRecord=conn.prepareStatement(sqlRoasterRecord);
	   rsRoasterRecord=psRoasterRecord.executeQuery();
	   
//Courses  
  PreparedStatement psCoursesRecord=null;
  ResultSet rsCoursesRecord =null;
  String sqlCoursesRecord =null;
  sqlCoursesRecord ="SELECT * FROM courses";
	   psCoursesRecord=conn.prepareStatement(sqlCoursesRecord);
	   rsCoursesRecord=psCoursesRecord.executeQuery();
	   
//professor 
  PreparedStatement psfacultyRecord=null;
  ResultSet rsfacultyRecord =null;
  String sqlfacultyRecord =null;
  sqlfacultyRecord ="SELECT * FROM faculty";
	   psfacultyRecord=conn.prepareStatement(sqlfacultyRecord);
	   rsfacultyRecord=psfacultyRecord.executeQuery(); 
	   
//this is for the student form where the student was ever enrolled
  PreparedStatement psEnrolledRecord=null;
  ResultSet rsEnrolledRecord =null;
  String sqlEnrolledRecord =null;
  sqlEnrolledRecord ="SELECT * FROM student";
	   psEnrolledRecord=conn.prepareStatement(sqlEnrolledRecord);
	   rsEnrolledRecord=psEnrolledRecord.executeQuery();
	  
  //this is for the student form where the student is enrolled graduate
  PreparedStatement psEnrolledMSRecord=null;
  ResultSet rsEnrolledMSRecord =null;
  String sqlEnrolledMSRecord =null;
  //sqlEnrolledMSRecord ="SELECT * FROM student s, graduate m where s.Enrolled = 'Y' AND s.Student_ID = m.Graduate_ID and m.Graduate_Type = 'MS'";
  sqlEnrolledMSRecord = 
		  "SELECT Distinct(s.Student_ID), s.First_Name, s.Middle_Initial,s.Last_Name, s.Enrolled, s.SSN, c.quarter, c.Class_Year " + 
		  "FROM student s, graduate m, enrolled e, classes c " +
		  "where s.Student_ID = m.Graduate_ID and m.Graduate_Type = 'MS' " +
			"  AND c.quarter = 'SPRING' " +
			"  AND c.Class_Year = '2009' and e.Section_ID = c.Section_ID " + 
			"  and s.Enrolled = 'y' "; 
	   psEnrolledMSRecord=conn.prepareStatement(sqlEnrolledMSRecord);
	   rsEnrolledMSRecord=psEnrolledMSRecord.executeQuery();
	   
  //graduate student degrees
  PreparedStatement psEnrolledMSDegreeRecord=null;
  ResultSet rsEnrolledMSDegreeRecord =null;
  String sqlEnrolledMSDegreeRecord =null;
  sqlEnrolledMSDegreeRecord ="SELECT d.Name, d.ID, m.Concentration FROM ms m, degree d where m.MS_ID = d.ID";
	   psEnrolledMSDegreeRecord=conn.prepareStatement(sqlEnrolledMSDegreeRecord);
	   rsEnrolledMSDegreeRecord=psEnrolledMSDegreeRecord.executeQuery();
	   
  //undergrad student
  PreparedStatement psEnrolledBSRecord=null;
  ResultSet rsEnrolledBSRecord =null;
  String sqlEnrolledBSRecord =null;
  //sqlEnrolledBSRecord ="SELECT * FROM student s, undergraduate u where s.Enrolled = 'Y' AND s.Student_ID = u.Undergraduate_ID";
  
  sqlEnrolledBSRecord = "SELECT Distinct(s.Student_ID), s.First_Name, s.Middle_Initial,s.Last_Name, s.Enrolled, s.SSN, c.quarter, c.Class_Year " +
							"FROM student s, undergraduate u, enrolled e, classes c "+
							"where s.Student_ID = u.Undergraduate_ID AND c.quarter = 'SPRING' "+
								  "AND c.Class_Year = '2009' and e.Section_ID = c.Section_ID "+
								  "and s.Enrolled = 'y' "; 
  
	   psEnrolledBSRecord=conn.prepareStatement(sqlEnrolledBSRecord);
	   rsEnrolledBSRecord=psEnrolledBSRecord.executeQuery();
	   
  //undergrad student degrees
  PreparedStatement psEnrolledBSDegreeRecord=null;
  ResultSet rsEnrolledBSDegreeRecord =null;
  String sqlEnrolledBSDegreeRecord =null;
  sqlEnrolledBSDegreeRecord ="SELECT d.Name, d.ID   FROM bs_ba b, degree d where b.BS_BA_ID = d.ID ";
	   psEnrolledBSDegreeRecord=conn.prepareStatement(sqlEnrolledBSDegreeRecord);
	   rsEnrolledBSDegreeRecord=psEnrolledBSDegreeRecord.executeQuery();
  
  %>
<html lang="en">
  <head>
  <!------------- updated version ---------------------> 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>
  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
		    <li role="presentation" class="active"><a href="report.jsp">Report</a></li>
            <li role="presentation"><a href="index.jsp">Form</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>
	  <!-------- Buttons ---------> 
	  
		<div class="btn-group" id ="moveLeft">
		  <a class="btn btn-Info btn-lg" href= "index.jsp" >
			<span class="glyphicon glyphicon-file" ></span> Input Form 
		  </a>
		</div>
		
		<div class="btn-group" id ="moveRight">
		  <button class="btn btn-warning btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
			<span class="glyphicon glyphicon-align-justify" ></span> Select Request <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
          <li><a href="#" id = "studentAction">Current Classes </a></li>
		  <li class="divider"></li>
          <li><a href="#" id = "gradeAction">Grade Report</a></li>
		  <li class="divider"></li>
		  <li><a href="#" id = "roasterAction">Roaster</a></li>
		  <li class="divider"></li>
		  <li><a href="#" id = "remainingAction">Remaining Req. BS</a></li>
		  <li class="divider"></li>
		  <li><a href="#" id = "remainingMSAction">Remaining Req. MS</a></li>
		  <li class="divider"></li>
		  <li><a href="#" id = "produceClass">Produce Class Schedule</a></li>
		  <li class="divider"></li>
		  <li><a href="#" id = "reviewPro">Assist Professor Review</a></li>
		  <li class="divider"></li>
		  <li><a href="#" id = "gradeDistri">Grade Distribution</a></li>
        </ul>
      </div>
	  <br></br>
	  <br></br>
	  <br></br>
     <div class="jumbotron" id ="hideJumbo">
        <h1>Submit Request</h1>
        <p class="lead">Select request above right.</p>		
      </div>
	  
	<BR></br>
	<BR></br>
	<!-- hide / show forms --> 
	 <div class="container">
	 <!-- Report II: --> 
	 <form role="group" action = "reviewEntry.jsp" id ="report2B" >
		<h1 align = "center" class="panel-title"><big><b>Assist Professor in Scheduling Review Session</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group has-success">
			<label>Section ID</label>
			<input required = "true" type="number" class="form-control" name = "ID" placeholder = "EX: 1">
		  </div>
		  <div class="form-group has-success">
			<label>Start</label>
			<input required = "true" type="text" class="form-control" name = "start" placeholder = "">
		  </div>
		  <div class="form-group has-success">
			<label>End</label>
			<input required = "true" type="text" class="form-control" name = "end" placeholder = "">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button>
		  <br></br>
		  <br></br>
		  <br></br>
	<div class="panel panel-success" >
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Showing Sections in current quarter SPRING 2009</h3>
   	 </div>

	<div class="table-responsive" id ="scrollFix">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Class Instructor</th>
				<th>Course Offering</th>
				<th>Section ID</th>
				<th>Title</th>
				<th>Quarter</th>
				<th>Class Year</th>
			  </tr>
			</thead>
			<tbody>
	<%
		//
	 int cnt=1;
	  while(rsSelectAssistRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectAssistRecord.getString("Class_Instructor")%> </td>
			  <td><%=rsSelectAssistRecord.getString("Course_offering")%> </td>
			  <td><%=rsSelectAssistRecord.getInt("Section_ID")%> </td>
			  <td><%=rsSelectAssistRecord.getString("title")%></td>
			  <td><%=rsSelectAssistRecord.getString("quarter")%></td>
			  <td><%=rsSelectAssistRecord.getInt("Class_Year")%></td>

			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	</form>
	 
	 <!-- Grade Distribution Report 3 --> 
	 <form role="group" action = "report3Request.jsp" id ="report3" >
		<h1 align = "center" class="panel-title"><big><b>Produce Grade Report Based on Input</b></big></h1>
		<BR></br>
		<legend><b>Input:</b>Course id (CID) X, a professor Y, and a quarter Z to produce the count of "A", "B", "C", "D", and "other" grades 
		that professor Y gave at quarter Z to the students taking course X </legend>
		<legend><b>Input:</b> Course id X and a professor Y to produce the count of "A", "B", "C", "D", and "other" 
				grades professor Y has given over the years</legend>
		<legend><b>Input:</b> a course id X to produce the count of "A", "B", "C", "D", and "other" grades given to students in X over the years</legend>
		<legend><b>Input:</b> a course id X and a professor Y to produce the grade point average that professor Y has given at course X over the years </legend>
		<BR></br>
		  <div class="form-group">
			<label>Course ID</label>
			<input required = "true" type="text" class="form-control" name = "CourseID" placeholder = "EX: 12345">
		  </div>
		  <div class="form-group">
			<label>Professor</label>
			<input type="text" class="form-control" name = "professor" placeholder = "EX: Armando">
		  </div>
		  <div class="form-group">
			<label>Quarter</label>
			<input type="text" class="form-control" name = "quarter" placeholder = "EX: Fall Or Spring Or Winter">
		  </div>
		  <div class="form-group">
			<label>Year</label>
			<input type="number" class="form-control" name = "year" placeholder = "EX: 2009">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="update">Update</button>
		  <br></br>
		  <br></br>
		  <br></br>
	<div class="panel panel-success" >
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Faculty</h3>
   	 </div>

	<div class="table-responsive" id ="scrollFix">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Name </th>
				<th>Title</th>
			  </tr>
			</thead>
			<tbody>
	<%
		//
	 cnt=1;
	  while(rsfacultyRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsfacultyRecord.getString("Name")%> </td>
			  <td><%=rsfacultyRecord.getString("Title")%> </td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	
	<div class="panel panel-danger" >
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Courses</h3>
   	 </div>

	<div class="table-responsive" id ="scrollFix">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Course ID</th>
				<th>Units</th>
				<th>Department Number</th>
			  </tr>
			</thead>
			<tbody>
	<%
		//
	  cnt=1;
	  while(rsCoursesRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsCoursesRecord.getString("Course_Number")%> </td>
			  <td><%=rsCoursesRecord.getString("Units")%> </td>
			  <td><%=rsCoursesRecord.getString("Department_Num")%> </td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	
	</form>
	 
	 <!-- Report II A: --> 
	 <form role="group" action = "studentSchedule.jsp" id ="report2" >
		<h1 align = "center" class="panel-title"><big><b>Assistance in producing Class Schedule</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group has-success">
			<label>Student ID</label>
			<input required = "true" type="text" class="form-control" name = "Student_ID" placeholder = "EX: a10003333">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button>
		  <br></br>
		  <br></br>
		  <br></br>
	<div class="panel panel-success" >
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Student Enrolled in current quarter 2009 of SPRING</h3>
   	 </div>

	<div class="table-responsive" id ="scrollFix">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>First Name</th>
				<th>Middle initial</th>
				<th>Last Name</th>
				<th>SSN</th>
				<th>Quarter</th>
				<th>Class Year</th>
			  </tr>
			</thead>
			<tbody>
	<%
		//
	 cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Student_ID")%> </td>
			  <td><%=rsSelectRecord.getString("First_Name")%> </td>
			  <td><%=rsSelectRecord.getString("Middle_Initial")%> </td>
			  <td><%=rsSelectRecord.getString("Last_Name")%></td>
			  <td><%=rsSelectRecord.getString("SSN")%></td>
			  <td><%=rsSelectRecord.getString("quarter")%></td>
			  <td><%=rsSelectRecord.getInt("Class_Year")%></td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	</form>
	 
	 
	 <!--Remaining degree requirements Master --> 
	 <form role="group" action = "remainingMSReport.jsp" id ="remainingMSForm" >
		<h1 align = "center" class="panel-title"><big><b>Remaining Degree Requirements Masters</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Student PID</label>
			<input required="true"  type="text" class="form-control" name ="PID">
		  </div>
		  <div class="form-group">
			<label>Name of Master's Degree</label>
			<input required="true"  type="text" class="form-control" name ="degreeName">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button> 
		  <br></br>
		  <br></br>
		<div class="panel panel-success">
		 <div class="panel-heading">
			<h3 class="panel-title">Query Results: Master Students Enrolled in Current Quarter: SPRING 2009</h3>
		 </div>

		<div class="table-responsive" id ="scrollFix">
			  <table class="table table-striped">
				<thead>
				  <tr>
					<th>#</th>
					<th>Student Id</th>
					<th>First Name</th>
					<th>Middle initial</th>
					<th>Last Name</th>
					<th>Enrolled</th> 
					<th>SSN</th>
					<th>Quarter</th>
					<th>Class Year</th>
				  </tr>
				</thead>
				<tbody>
		<%
		  cnt=1;
		  while(rsEnrolledMSRecord.next())
		  {
		  %>
		   <tr>
				  <th scope="row"><%=cnt%></th>
				  <td><%=rsEnrolledMSRecord.getString("Student_ID")%> </td>
				  <td><%=rsEnrolledMSRecord.getString("First_Name")%> </td>
				  <td><%=rsEnrolledMSRecord.getString("Middle_Initial")%> </td>
				  <td><%=rsEnrolledMSRecord.getString("Last_Name")%></td>
				  <td><%=rsEnrolledMSRecord.getString("Enrolled")%></td>
				  <td><%=rsEnrolledMSRecord.getString("SSN")%></td>
				  <td><%=rsEnrolledMSRecord.getString("quarter")%></td>
				  <td><%=rsEnrolledMSRecord.getString("Class_Year")%></td>
				  </tr>
				   <%
		   cnt++;   /// increment of counter
		  } /// End of while loop
		  %>
				</tbody>
			  </table>
			</div>
		</div>
		<br></br>
		<br></br>
		<div class="panel panel-success">
		 <div class="panel-heading">
			<h3 class="panel-title">Query Results: Master Degrees</h3>
		 </div>

		<div class="table-responsive" id ="scrollFix">
			  <table class="table table-striped">
				<thead>
				  <tr>
					<th>#</th>
					<th>Degree Name</th>
					<th>ID</th>
					<th>MS</th>
					<th>Concentration</th>
				  </tr>
				</thead>
				<tbody>
				<%
		   cnt=1;
		  while(rsEnrolledMSDegreeRecord.next())
		  {
		  %>
		   <tr>
				  <th scope="row"><%=cnt%></th>
				  <td><%=rsEnrolledMSDegreeRecord.getString("Name")%> </td>
				  <td><%=rsEnrolledMSDegreeRecord.getString("ID")%> </td>
				  <td>MS</td>
				  <td><%=rsEnrolledMSDegreeRecord.getString("Concentration")%> </td>
			</tr>
				   <%
		   cnt++;   /// increment of counter
		  } /// End of while loop
		  %>
				</tbody>
			  </table>
			</div>
		</div>
	</form>
	 
	 
	 <!-- Remaining degree requirements Bachelors--> 
	 <form role="group" action = "remainingReport.jsp" id ="remainingForm" >
		<h1 align = "center" class="panel-title"><big><b>Remaining Degree Requirements Bachelors</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Student PID</label>
			<input required="true"  type="text" class="form-control" name ="PID">
		  </div>
		  <div class="form-group">
			<label>Name of Degree</label>
			<input required="true"  type="text" class="form-control" name ="degreeName">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button> 
		  <br></br>
		  <br></br>
		<div class="panel panel-success">
		 <div class="panel-heading">
			<h3 class="panel-title">Query Results: Students Enrolled Bachelor in current Quarter: SPRING 2009</h3>
		 </div>

		<div class="table-responsive" id ="scrollFix">
			  <table class="table table-striped">
				<thead>
				  <tr>
					<th>#</th>
					<th>Student Id</th>
					<th>First Name</th>
					<th>Middle initial</th>
					<th>Last Name</th>
					<th>Enrolled</th> 
					<th>SSN</th>
					<th>Quarter</th>
					<th>Year</th> 
				  </tr>
				</thead>
				<tbody>
		<%
		  cnt=1;
		  while(rsEnrolledBSRecord.next())
		  {
		  %>
		   <tr>
				  <th scope="row"><%=cnt%></th>
				  <td><%=rsEnrolledBSRecord.getString("Student_ID")%> </td>
				  <td><%=rsEnrolledBSRecord.getString("First_Name")%> </td>
				  <td><%=rsEnrolledBSRecord.getString("Middle_Initial")%> </td>
				  <td><%=rsEnrolledBSRecord.getString("Last_Name")%></td>
				  <td><%=rsEnrolledBSRecord.getString("Enrolled")%></td>
				  <td><%=rsEnrolledBSRecord.getString("SSN")%></td>
				  <td><%=rsEnrolledBSRecord.getString("quarter")%></td>
				  <td><%=rsEnrolledBSRecord.getInt("Class_Year")%></td>
				  </tr>
				   <%
		   cnt++;   /// increment of counter
		  } /// End of while loop
		  %>
				</tbody>
			  </table>
			</div>
		</div>
		<br></br>
		<br></br>
		<div class="panel panel-success">
		 <div class="panel-heading">
			<h3 class="panel-title">Query Results: Major and Type</h3>
		 </div>

		<div class="table-responsive" id ="scrollFix">
			  <table class="table table-striped">
				<thead>
				  <tr>
					<th>#</th>
					<th>Degree Name</th>
					<th>ID</th>
					<th>Type</th>
				  </tr>
				</thead>
				<tbody>
				<%
		  cnt=1;
		  while(rsEnrolledBSDegreeRecord.next())
		  {
		  %>
		   <tr>
				  <th scope="row"><%=cnt%></th>
				  <td><%=rsEnrolledBSDegreeRecord.getString("Name")%> </td>
				  <td><%=rsEnrolledBSDegreeRecord.getString("ID")%> </td>
				  <td>BS</td>
				  </tr>
				   <%
		   cnt++;   /// increment of counter
		  } /// End of while loop
		  %>
				</tbody>
			  </table>
			</div>
		</div>
	</form>
	 
	 
	<!-- Grade Report --> 
	<form role="group" action = "gradeReport.jsp" id ="gradeForm" >
		<h1 align = "center" class="panel-title"><big><b>Grade Report for Student</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label >Student PID</label>
			<input required="true"  type="text" class="form-control" name ="PID">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button> 
		  <br></br>
		  <br></br>
		<div class="panel panel-success" id = "studentTable">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Students Ever Enrolled</h3>
   	 </div>

	<div class="table-responsive" id ="scrollFix">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>First Name</th>
				<th>Middle initial</th>
				<th>Last Name</th>
				<th>Enrolled</th> 
				<th>SSN</th>
			  </tr>
			</thead>
			<tbody>
	<%
	   cnt=1;
	  while(rsEnrolledRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsEnrolledRecord.getString("Student_ID")%> </td>
			  <td><%=rsEnrolledRecord.getString("First_Name")%> </td>
			  <td><%=rsEnrolledRecord.getString("Middle_Initial")%> </td>
			  <td><%=rsEnrolledRecord.getString("Last_Name")%></td>
			  <td><%=rsEnrolledRecord.getString("Enrolled")%></td>
			  <td><%=rsEnrolledRecord.getString("SSN")%></td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	</form>
	
	<!-- Roaster of class--> 
		<form role="group" action = "ClassRoster.jsp" id ="roasterForm" >
		<h1 align = "center" class="panel-title"><big><b>Roaster of Class</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group">
			<label>Class Title</label>
			<input required = "true" type="text" class="form-control" name = "title" >
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button>
		  <br></br>
		  <br></br>
		  <br></br>
		  <!-- Show CLasses table --> 
			<div class="panel panel-success" id = "studentTable">
			 <div class="panel-heading">
				<h3 class="panel-title">Query Results: Classes</h3>
			 </div>
			<div class="table-responsive" id ="scrollFix">
				  <table class="table table-striped">
					<thead>
					  <tr>
						<th>#</th>
						<th>Class Instructor</th>
						<th>Course Offering</th>
						<th>Section ID</th>
						<th>Title</th>
						<th>Quarter</th>
						<th>Class Year</th>
						<th>Enrollment Limit</th>
					  </tr>
					</thead>
					<tbody>
			<%
			  cnt=1;
			  while(rsRoasterRecord.next())
			  {
			  %>
			   <tr>
					  <th scope="row"><%=cnt%></th>
					  <td><%=rsRoasterRecord.getString("Class_Instructor")%> </td>
					  <td><%=rsRoasterRecord.getString("Course_offering")%> </td>
					  <td><%=rsRoasterRecord.getString("Section_ID")%> </td>
					  <td><%=rsRoasterRecord.getString("title")%></td>
					  <td><%=rsRoasterRecord.getString("quarter")%></td>
					  <td><%=rsRoasterRecord.getString("Class_Year")%> </td>
					  <td><%=rsRoasterRecord.getString("Enrollment_Limit")%></td>
					  </tr>
					   <%
			   cnt++;   /// increment of counter
			  } /// End of while loop
			  %>
					</tbody>
				  </table>
				</div>
			</div>
	    </form>	
	
	
	<!-- student request input column --> 
	<form role="group" action = "StudentEnrollment.jsp" id ="studentForm" >
		<h1 align = "center" class="panel-title"><big><b>Classes Currently Taken By Student Request</b></big></h1>
		<BR></br>
		<BR></br>
		  <div class="form-group has-success">
			<label>Student ID</label>
			<input required = "true" type="text" class="form-control" name = "Student_ID" placeholder = "EX: a10003333">
		  </div>
		  <button type="submit" class="btn btn-default" name ="Submit" value ="request">Submit Request</button>
		  <br></br>
		  <br></br>
		  <br></br>
	<div class="panel panel-success" id = "studentTable">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: Student Enrolled in current quarter 2009 of SPRING</h3>
   	 </div>

	<div class="table-responsive" id ="scrollFix">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>First Name</th>
				<th>Middle initial</th>
				<th>Last Name</th>
				<th>SSN</th>
				<th>Quarter</th>
				<th>Class Year</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  cnt=1;
	  while(rsSelectRecord1.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord1.getString("Student_ID")%> </td>
			  <td><%=rsSelectRecord1.getString("First_Name")%> </td>
			  <td><%=rsSelectRecord1.getString("Middle_Initial")%> </td>
			  <td><%=rsSelectRecord1.getString("Last_Name")%></td>
			  <td><%=rsSelectRecord1.getString("SSN")%></td>
			  <td><%=rsSelectRecord1.getString("quarter")%></td>
			  <td><%=rsSelectRecord1.getInt("Class_Year")%></td>

			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
	</div>
	</form>
		
</div> <!-- container wrapped around tables --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->


    <script>

	var ids = ["studentForm","roasterForm","hideJumbo","gradeForm","remainingForm", 
				"remainingMSForm", "report2", "report3", "report2B"];
			   
	var i;
	for(i =0; i< ids.length; i++){
		if(ids[i] != "hideJumbo")
		document.getElementById(ids[i]).style.display = 'none'; 
	}
	
	//reviewPro
	jQuery("#reviewPro").click(function(e){
		if(document.getElementById('report2B').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('report2B').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//produceClass for report III: 
	jQuery("#gradeDistri").click(function(e){
		if(document.getElementById('report3').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('report3').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//produceClass for report II: 
	jQuery("#produceClass").click(function(e){
	//do something
		if(document.getElementById('report2').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('report2').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	//Remaining Reqquiremen for Master students 
	jQuery("#remainingMSAction").click(function(e){
	//do something
		if(document.getElementById('remainingMSForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('remainingMSForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	//Remaining Degree req. Bachelors 
	jQuery("#remainingAction").click(function(e){
	//do something
		if(document.getElementById('remainingForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('remainingForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//gradeForm
	jQuery("#gradeAction").click(function(e){
	//do something
		if(document.getElementById('gradeForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('gradeForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	//takes care of showing / make forms disappear 
	jQuery("#studentAction").click(function(e){
	//do something
		if(document.getElementById('studentForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('studentForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});
	
	
	jQuery("#roasterAction").click(function(e){
	//do something
		if(document.getElementById('roasterForm').style.display == 'none'){
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById('roasterForm').style.display='';
		}
		else{
			for(i =0; i< ids.length; i++){
				document.getElementById(ids[i]).style.display = 'none'; 
			}
			document.getElementById("hideJumbo").style.display = ''; 
		}
		e.preventDefault();
	});

	</script>
  </body>
</html>

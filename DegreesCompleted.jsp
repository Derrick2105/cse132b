<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
        
  String StudentID =request.getParameter("Student_ID");
  String DegreeType = request.getParameter("Degree_Type");
  String name = request.getParameter("Degree_Name");
  String school = request.getParameter("Degree_School");
  // Set any empty strings to null so that sql doesn't accept bad requests
  if (StudentID.length()!=9)
	 StudentID = null;
  if (DegreeType.length() == 0) 
	 DegreeType = null;		
   if (name.length()==0)
	 name = null;
  if (school.length() == 0) 
	 school = null;		



  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	   conn.setAutoCommit(false);
	   sqlInsertRecord="insert into DegreesCompleted (Degree_Type,Degree_Name, Degree_School) values (?,?,?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,DegreeType);
	   psInsertRecord.setString(2,name);
	   psInsertRecord.setString(3,school);				
	   psInsertRecord.executeUpdate();
	   
	   sqlInsertRecord="insert into StudentDegrees (Student_ID,Degree_Type,Degree_Name, Degree_School) values (?,?,?,?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,StudentID);
	   psInsertRecord.setString(2,DegreeType);
	   psInsertRecord.setString(3,name);
	   psInsertRecord.setString(4,school);	
	   psInsertRecord.executeUpdate();
	   
       conn.commit();
	   conn.setAutoCommit(true);	   
	   sqlSelectRecord ="Select * from StudentDegrees where Student_ID = ? and Degree_Type = ? and Degree_Name = ? and Degree_School = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,StudentID);
	   psInsertRecord.setString(2,DegreeType);
	   psInsertRecord.setString(3,name);
	   psInsertRecord.setString(4,school);	
	   rsSelectRecord=psInsertRecord.executeQuery();

   } else if(buttonDec.equals("delete")){
	   sqlSelectRecord ="Delete from DegreesCompleted where Degree_Type = ? and Degree_Name = ? and Degree_School = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,DegreeType);
	   psInsertRecord.setString(2,name);
	   psInsertRecord.setString(3,school);
	   psInsertRecord.executeUpdate();
	   
	   System.out.println("Here!");
	   sqlSelectRecord ="Select * from StudentDegrees where Student_ID = ? and Degree_Type = ? and Degree_Name = ? and Degree_School = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setString(1,StudentID);
	   psInsertRecord.setString(2,DegreeType);
	   psInsertRecord.setString(3,name);
	   psInsertRecord.setString(4,school);	
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   
  }
  catch(Exception e)
  {
      if (conn != null) {
         System.err.print("Transaction is being rolled back");
         //conn.rollback();
      }
	  conn.setAutoCommit(true);
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Student Id</th>
				<th>Degree Type</th>
				<th>Degree Name</th>
				<th>School</th>

			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Student_ID")%> </td>
			  <td><%=rsSelectRecord.getString("Degree_Type")%> </td>
			  <td><%=rsSelectRecord.getString("Degree_Name")%> </td>
			  <td><%=rsSelectRecord.getString("Degree_School")%> </td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
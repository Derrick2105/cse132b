<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%
  Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
        
  String Instructor =request.getParameter("Instructor");
  String Offering = request.getParameter("Offering");
  String SectionAsString = request.getParameter("Section");
  String Title = request.getParameter("Title");
  String Quarter = request.getParameter("Quarter");
  String YearAsString = request.getParameter("Year");
  String EnrollmentLimitAsString = request.getParameter("Enrollment_Limit");
  String start = request.getParameter("start");
  String endD = request.getParameter("end");
  
  
  int section = 0;
  int year = 0;
  int EnrollmentLimit = 0;
  try {
	// Set any empty strings to null so that sql doesn't accept bad requests
	if (Instructor.length()==0)
	    Instructor = null;
	if (Offering.length()==0)
		Offering = null;
	if (Title.length() == 0)
        Title = null;		
	if (Quarter.length() == 0)
        Quarter = null;		
	if(start.length() == 0)
		start = null; 
	if(endD.length() == 0){
		endD = null; 
	}
	section = Integer.parseInt(SectionAsString.trim()); 
	year = Integer.parseInt(YearAsString.trim()); 
	EnrollmentLimit = Integer.parseInt(EnrollmentLimitAsString.trim()); 
  } catch(NumberFormatException badSSN){
	  System.out.println("Do something with this!!!");
  }

  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	System.out.println("buttonDec was addddddd");
	   sqlInsertRecord="insert into Classes (Class_Instructor, Course_Offering, Section_ID, title, quarter,startDate, endDate ,Class_Year, Enrollment_Limit) values(?,?,?,?,?,?,?,?,?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,Instructor);
	   psInsertRecord.setString(2,Offering);
	   psInsertRecord.setInt(3,section);
	   psInsertRecord.setString(4,Title);
	   psInsertRecord.setString(5,Quarter);
	   psInsertRecord.setString(6,start); 
	   psInsertRecord.setString(7,endD); 
	   psInsertRecord.setInt(8,year);
	   psInsertRecord.setInt(9,EnrollmentLimit);
				
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="SELECT * FROM Classes";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   else if(buttonDec.equals("update")){
	   sqlInsertRecord="update Classes set Class_Instructor = ?, Course_Offering =?, title = ?,quarter =?, startDate = ?, endDate = ?,Class_Year=?, Enrollment_Limit = ? where Section_ID = ? ";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setString(1,Instructor);
	   psInsertRecord.setString(2,Offering);
	   psInsertRecord.setString(3,Title);
	   psInsertRecord.setString(4,Quarter);
	   psInsertRecord.setString(5,start); 
	   psInsertRecord.setString(6,endD); 
	   psInsertRecord.setInt(7,year);
	   psInsertRecord.setInt(8,EnrollmentLimit);
	   psInsertRecord.setInt(9,section);

				
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="SELECT * FROM classes";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
	   System.out.println("I entered update"); 
   }
   else if(buttonDec.equals("delete")){
	   sqlInsertRecord="Delete from Classes where Section_ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,section);
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="SELECT * FROM classes";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   
  }
  catch(Exception e)
  {
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Class Instructor</th>
				<th>Course Offering</th>
				<th>Section</th>
				<th>Title</th>
				<th>Quarter</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Year</th>
				<th>Enrollment Limit</th>
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Class_Instructor")%> </td>
			  <td><%=rsSelectRecord.getString("Course_Offering")%> </td>
			  <td><%=rsSelectRecord.getString("Section_ID")%> </td>
			  <td><%=rsSelectRecord.getString("title")%></td>
			  <td><%=rsSelectRecord.getString("quarter")%></td>
			  <td><%=rsSelectRecord.getString("startDate")%></td>
			  <td><%=rsSelectRecord.getString("endDate")%></td>
			  <td><%=rsSelectRecord.getString("Class_Year")%></td>
			  <td><%=rsSelectRecord.getString("Enrollment_Limit")%></td>
		</tr>
	  <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>
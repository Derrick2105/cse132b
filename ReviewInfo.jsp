<%@ page language="java" import="java.sql.*, java.util.Properties,java.io.*,java.lang.*" errorPage="" %>
<%Connection conn = null;
  Properties properties = new Properties();
  properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
  String url = properties.getProperty("jdbc.url");
  String driver = properties.getProperty("jdbc.driver");
  String username = properties.getProperty("jdbc.username");
  String password = properties.getProperty("jdbc.password");
  if (driver == "com.mysql.jdbc.Driver")
	Class.forName(driver).newInstance();
  else 
    Class.forName(driver);
  conn = DriverManager.getConnection(url,username,password);
 
  PreparedStatement psInsertRecord=null;
  ResultSet rsSelectRecord =null;
  String sqlSelectRecord =null;
  
  String sqlInsertRecord=null;
        
  String Days =request.getParameter("Days");
  String SectionAsString = request.getParameter("Section_ID");
  String ReviewAsString = request.getParameter("Review_ID");
  String MeetingAsString = request.getParameter("Meeting_time");
  String Location = request.getParameter("Location");
  String reviewDay = request.getParameter("reviewDay");
  
  int Section = 0;
  int Review = 0;
  int MeetingTime =0;
  
  try {
	// Set any empty strings to null so that sql doesn't accept bad requests
	if (Days.length() == 0)
	    Days = null;
	if (Location.length() == 0) 
		Location = null;		
	if(reviewDay.length() == 0)
		reviewDay = null; 
	Section = Integer.parseInt(SectionAsString); 
	Review = Integer.parseInt(ReviewAsString); 
	MeetingTime = Integer.parseInt(MeetingAsString); 
	
  } catch(NumberFormatException badSSN){
	  System.out.println("Do something with this!!!");
  }

  //for the buttons portion that decides whether to update,delete,addRecord
  String buttonDec = request.getParameter("Submit"); 
  
 // int staffPhone=Integer.parseInt(request.getParameter("sPhone"));
         //// if this come blank and alpha number it will throw parse int 
            //NumberFormatException exception, only number
        
  try
  {
  
   if( buttonDec == null ){ 
   System.out.println("buttonDec was nullllll");
   
   }
   else if(buttonDec.equals("add")){
	   conn.setAutoCommit(false);
	   sqlInsertRecord="insert into Meetings (ID,Meeting_time, Meeting_Days, Location) values(?,?,?,?)";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,Review);
	   psInsertRecord.setInt(2,MeetingTime);
	   psInsertRecord.setString(3,Days);
	   psInsertRecord.setString(4,Location);				
	   psInsertRecord.executeUpdate();
	   
	   sqlInsertRecord="insert into ClassMeetings (Section_ID,Meet_ID) values (?,?)";
       psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,Section);
	   psInsertRecord.setInt(2,Review);
	   psInsertRecord.executeUpdate();
	   
	   sqlInsertRecord="insert into Review (Review_ID, day) values (?,?)";
       psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,Review);
	   psInsertRecord.getString(2,reviewDay); 
	   psInsertRecord.executeUpdate();
			   
	   sqlSelectRecord ="Select * from Review where Review_ID = ? and day = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,Review);
	   psInsertRecord.getString(2,reviewDay); 
	   rsSelectRecord=psInsertRecord.executeQuery();
	   conn.commit();
	   conn.setAutoCommit(true);

   }
   else if(buttonDec.equals("update")){
	   sqlInsertRecord="update Meetings set Meeting_time = ?, Meeting_Days = ?, Location = ? where ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlInsertRecord);
	   psInsertRecord.setInt(1,MeetingTime);
	   psInsertRecord.setString(2,Days);
	   psInsertRecord.setString(3,Location);
	   psInsertRecord.setInt(4,Review);
				
	   psInsertRecord.executeUpdate();
        
	   //this does not work but were leaving it there for now 
	   sqlSelectRecord ="Select * from Review where Review_ID = ? and day = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,Review);
	   psInsertRecord.setString(2,day); 
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   else if(buttonDec.equals("delete")){
	   sqlSelectRecord ="Delete from Meetings where ID = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,Review);
	   psInsertRecord.executeUpdate();
	   
	   sqlSelectRecord ="Select * from Review where Review_ID = ? and day = ?";
	   psInsertRecord=conn.prepareStatement(sqlSelectRecord);
	   psInsertRecord.setInt(1,Review);
	   psInsertRecord.getString(2,reviewDay); 
	   rsSelectRecord=psInsertRecord.executeQuery();
   }
   
  }
  catch(Exception e)
  {
      if (conn != null) {
         System.err.print("Transaction is being rolled back");
         conn.rollback();
      }
	  conn.setAutoCommit(true);
	  e.printStackTrace();
      response.sendRedirect("index.jsp");//// On error it will send back to addRecord.jsp page
	  return; 
  }
        
%>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Class Project</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
	 <link href="buttons.css" rel="stylesheet">
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	 <script src="bootstrap.min.js"></script>

  </head>

  <body>
    <div class="container" id ="adjust">
      <div class="header">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation" ><a href="index.jsp">Home</a></li>
            <li role="presentation"><a href="about.html">About</a></li>
          </ul>
        </nav>
        <h3 id = "blueColor" class="text-muted">CSE 132B DataBase Class Project </h3>
		<sup>Authors: Armando Alvarado and Derrick Smith</sup>
      </div>

<a href="index.jsp" class="btn btn-info btn-lg"><span class="glyphicon glyphicon-hand-left"></span>  Go Back</a>

   <center><h3 id = "blueColor" class="text-muted" ><b>Successful Query!</b></h3></center>
   <br>
   <br>
   <br>
	<div class="panel panel-success">
	 <div class="panel-heading">
		<h3 class="panel-title">Query Results: </h3>
   	 </div>
	<div class="table-responsive">
		  <table class="table table-striped">
			<thead>
			  <tr>
				<th>#</th>
				<th>Review Id</th>
				<th>Review Day</th> 
			  </tr>
			</thead>
			<tbody>
	<%
	  int cnt=1;
	  while(rsSelectRecord.next())
	  {
	  %>
	   <tr>
			  <th scope="row"><%=cnt%></th>
			  <td><%=rsSelectRecord.getString("Review_ID")%> </td
			  <td><%=rsSelectRecord.getString("day")%> </td>
			  </tr>
			   <%
	   cnt++;   /// increment of counter
	  } /// End of while loop
	  %>
			</tbody>
		  </table>
		</div>
		</div>
			</div> <!--container --> 


    <!--  <footer class="footer">
        <p> Contact aalvardo7818@gmail.com </p>
      </footer>
-->
   


  </body>
</html>
<%
    try{
      if(psInsertRecord!=null)
      {
       psInsertRecord.close();
      }
	    if(rsSelectRecord!=null)
          {
            rsSelectRecord.close();
          }
      
      if(conn!=null)
      {
       conn.close();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace(); 
    }
%>